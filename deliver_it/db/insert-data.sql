-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.3-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table deliver_it.addresses: ~7 rows (approximately)
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` (`address_id`, `street_name`, `city_id`) VALUES
(1, 'Sofia Street Name', 1),
(2, 'Ihtiman Street Name', 2),
(3, 'Pleven Street Name', 3),
(4, 'Plovdiv Street Name', 4),
(5, 'Varna Street Name', 5),
(6, 'Burgas Street Name', 6),
(7, 'Telerik Street', 1);
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;

-- Dumping data for table deliver_it.categories: ~5 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`category_id`, `category_name`) VALUES
(1, 'Electronics'),
(2, 'Clothing'),
(3, 'Medical'),
(4, 'Sports'),
(5, 'Outdoor');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping data for table deliver_it.cities: ~6 rows (approximately)
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` (`city_id`, `city_name`, `country_id`) VALUES
(1, 'Sofia', 1),
(2, 'Ihtiman', 1),
(3, 'Pleven', 1),
(4, 'Plovdiv', 1),
(5, 'Varna', 1),
(6, 'Burgas', 1);
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;

-- Dumping data for table deliver_it.countries: ~0 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`country_id`, `country_name`) VALUES
(1, 'Bulgaria');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping data for table deliver_it.delivery_types: ~2 rows (approximately)
/*!40000 ALTER TABLE `delivery_types` DISABLE KEYS */;
INSERT INTO `delivery_types` (`delivery_type_id`, `delivery_type_name`) VALUES
(1, 'Pick up from warehouse'),
(2, 'Deliver to address');
/*!40000 ALTER TABLE `delivery_types` ENABLE KEYS */;

-- Dumping data for table deliver_it.parcels: ~2 rows (approximately)
/*!40000 ALTER TABLE `parcels` DISABLE KEYS */;
INSERT INTO `parcels` (`parcel_id`, `user_id`, `warehouse_id`, `weight`, `delivery_type_id`, `shipment_id`, `category_id`) VALUES
(1, 3, 1, 20, 1, 1, 5),
(2, 4, 3, 50, 1, 2, 4);
/*!40000 ALTER TABLE `parcels` ENABLE KEYS */;

-- Dumping data for table deliver_it.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `role_name`) VALUES
(1, 'Customer'),
(2, 'Employee');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping data for table deliver_it.shipments: ~2 rows (approximately)
/*!40000 ALTER TABLE `shipments` DISABLE KEYS */;
INSERT INTO `shipments` (`shipment_id`, `departure_date`, `arrival_date`, `origin_warehouse_id`, `destination_warehouse_id`, `status_id`) VALUES
(1, '2021-08-16 16:40:41', '2021-08-17 16:40:44', 1, 2, 1),
(2, '2021-08-16 16:41:14', '2021-08-18 16:41:17', 1, 3, 1);
/*!40000 ALTER TABLE `shipments` ENABLE KEYS */;

-- Dumping data for table deliver_it.statuses: ~3 rows (approximately)
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` (`status_id`, `status_name`) VALUES
(1, 'Preparing'),
(2, 'On the way'),
(3, 'Completed');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;

-- Dumping data for table deliver_it.users: ~5 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `password`, `email`, `role_id`) VALUES
(1, 'Preslav', 'Hristov', 'pass', 'ph@email.com', 2),
(2, 'Borislav', 'Kibarov', 'pass', 'bk@email.com', 2),
(3, 'Petar', 'Raykov', 'pass', 'pr@email.com', 1),
(4, 'Todor', 'Andonov', 'pass', 'ta@email.com', 1),
(5, 'Vladimir', 'Venkov', 'pass', 'vv@email.com', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping data for table deliver_it.user_addresses: ~4 rows (approximately)
/*!40000 ALTER TABLE `user_addresses` DISABLE KEYS */;
INSERT INTO `user_addresses` (`user_address_id`, `user_id`, `address_id`) VALUES
(1, 1, 3),
(2, 3, 7),
(3, 4, 7),
(4, 5, 7);
/*!40000 ALTER TABLE `user_addresses` ENABLE KEYS */;

-- Dumping data for table deliver_it.warehouses: ~6 rows (approximately)
/*!40000 ALTER TABLE `warehouses` DISABLE KEYS */;
INSERT INTO `warehouses` (`warehouse_id`, `address_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6);
/*!40000 ALTER TABLE `warehouses` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
