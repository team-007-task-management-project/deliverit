-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.3-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;


-- Dumping database structure for deliver_it
CREATE DATABASE IF NOT EXISTS `deliver_it` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `deliver_it`;

-- Dumping structure for table deliver_it.addresses
CREATE TABLE IF NOT EXISTS `addresses`
(
    `address_id`  int(11)     NOT NULL AUTO_INCREMENT,
    `street_name` varchar(50) NOT NULL,
    `city_id`     int(11)     NOT NULL,
    PRIMARY KEY (`address_id`),
    UNIQUE KEY `addresses_address_id_uindex` (`address_id`),
    KEY `addresses_cities_city_id_fk` (`city_id`),
    CONSTRAINT `addresses_cities_city_id_fk` FOREIGN KEY (`city_id`) REFERENCES `cities` (`city_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 14
  DEFAULT CHARSET = latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliver_it.categories
CREATE TABLE IF NOT EXISTS `categories`
(
    `category_id`   int(11)     NOT NULL AUTO_INCREMENT,
    `category_name` varchar(30) NOT NULL,
    PRIMARY KEY (`category_id`),
    UNIQUE KEY `categories_category_id_uindex` (`category_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliver_it.cities
CREATE TABLE IF NOT EXISTS `cities`
(
    `city_id`    int(11)     NOT NULL AUTO_INCREMENT,
    `city_name`  varchar(20) NOT NULL,
    `country_id` int(11)     NOT NULL,
    PRIMARY KEY (`city_id`),
    KEY `cities_countries_country_id_fk` (`country_id`),
    CONSTRAINT `cities_countries_country_id_fk` FOREIGN KEY (`country_id`) REFERENCES `countries` (`country_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 7
  DEFAULT CHARSET = latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliver_it.countries
CREATE TABLE IF NOT EXISTS `countries`
(
    `country_id`   int(11)     NOT NULL AUTO_INCREMENT,
    `country_name` varchar(20) NOT NULL,
    PRIMARY KEY (`country_id`),
    UNIQUE KEY `countries_country_name_uindex` (`country_name`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliver_it.delivery_types
CREATE TABLE IF NOT EXISTS `delivery_types`
(
    `delivery_type_id`   int(11)     NOT NULL AUTO_INCREMENT,
    `delivery_type_name` varchar(30) NOT NULL,
    PRIMARY KEY (`delivery_type_id`),
    UNIQUE KEY `delivery_types_delivery_type_id_uindex` (`delivery_type_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliver_it.parcels
CREATE TABLE IF NOT EXISTS `parcels`
(
    `parcel_id`        int(11) NOT NULL AUTO_INCREMENT,
    `user_id`          int(11) NOT NULL,
    `warehouse_id`     int(11) NOT NULL,
    `weight`           double  NOT NULL,
    `delivery_type_id` int(11) NOT NULL,
    `shipment_id`      int(11) NOT NULL,
    `category_id`      int(11) NOT NULL,
    PRIMARY KEY (`parcel_id`),
    KEY `parcels_categories_category_id_fk` (`category_id`),
    KEY `parcels_shipments_shipment_id_fk` (`shipment_id`),
    KEY `parcels_users_user_id_fk` (`user_id`),
    KEY `parcels_warehouses_warehouse_id_fk` (`warehouse_id`),
    KEY `parcels_delivery_types_delivery_type_id_fk` (`delivery_type_id`),
    CONSTRAINT `parcels_categories_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`),
    CONSTRAINT `parcels_delivery_types_delivery_type_id_fk` FOREIGN KEY (`delivery_type_id`) REFERENCES `delivery_types` (`delivery_type_id`),
    CONSTRAINT `parcels_shipments_shipment_id_fk` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`shipment_id`),
    CONSTRAINT `parcels_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
    CONSTRAINT `parcels_warehouses_warehouse_id_fk` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`warehouse_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliver_it.roles
CREATE TABLE IF NOT EXISTS `roles`
(
    `role_id`   int(11)     NOT NULL AUTO_INCREMENT,
    `role_name` varchar(15) NOT NULL,
    PRIMARY KEY (`role_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliver_it.shipments
CREATE TABLE IF NOT EXISTS `shipments`
(
    `shipment_id`              int(11)  NOT NULL AUTO_INCREMENT,
    `departure_date`           datetime NOT NULL,
    `arrival_date`             datetime NOT NULL,
    `origin_warehouse_id`      int(11)  NOT NULL,
    `destination_warehouse_id` int(11)  NOT NULL,
    `status_id`                int(11)  NOT NULL,
    PRIMARY KEY (`shipment_id`),
    KEY `shipments_warehouses_warehouse_id_fk` (`origin_warehouse_id`),
    KEY `shipments_warehouses_warehouse_id_fk_2` (`destination_warehouse_id`),
    KEY `shipments___fk` (`status_id`),
    CONSTRAINT `shipments___fk` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`status_id`),
    CONSTRAINT `shipments_warehouses_warehouse_id_fk` FOREIGN KEY (`origin_warehouse_id`) REFERENCES `warehouses` (`warehouse_id`),
    CONSTRAINT `shipments_warehouses_warehouse_id_fk_2` FOREIGN KEY (`destination_warehouse_id`) REFERENCES `warehouses` (`warehouse_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliver_it.statuses
CREATE TABLE IF NOT EXISTS `statuses`
(
    `status_id`   int(11)     NOT NULL AUTO_INCREMENT,
    `status_name` varchar(30) NOT NULL,
    PRIMARY KEY (`status_id`),
    UNIQUE KEY `statuses_status_id_uindex` (`status_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliver_it.users
CREATE TABLE IF NOT EXISTS `users`
(
    `user_id`    int(11)     NOT NULL AUTO_INCREMENT,
    `first_name` varchar(30) NOT NULL,
    `last_name`  varchar(30) NOT NULL,
    `password`   varchar(15) NOT NULL,
    `email`      varchar(50) NOT NULL,
    `role_id`    int(11)     NOT NULL,
    PRIMARY KEY (`user_id`),
    UNIQUE KEY `users_email_uindex` (`email`),
    UNIQUE KEY `users_user_id_uindex` (`user_id`),
    KEY `users_roles_role_id_fk` (`role_id`),
    CONSTRAINT `users_roles_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 9
  DEFAULT CHARSET = latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliver_it.user_addresses
CREATE TABLE IF NOT EXISTS `user_addresses`
(
    `user_address_id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id`         int(11) DEFAULT NULL,
    `address_id`      int(11) DEFAULT NULL,
    PRIMARY KEY (`user_address_id`),
    UNIQUE KEY `user_addresses_user_address_id_uindex` (`user_address_id`),
    KEY `user_addresses_addresses_address_id_fk` (`address_id`),
    KEY `user_addresses_users_user_id_fk` (`user_id`),
    CONSTRAINT `user_addresses_addresses_address_id_fk` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`address_id`),
    CONSTRAINT `user_addresses_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 8
  DEFAULT CHARSET = latin1;

-- Data exporting was unselected.

-- Dumping structure for table deliver_it.warehouses
CREATE TABLE IF NOT EXISTS `warehouses`
(
    `warehouse_id` int(11) NOT NULL AUTO_INCREMENT,
    `address_id`   int(11) NOT NULL,
    PRIMARY KEY (`warehouse_id`),
    KEY `warehouses_addresses_address_id_fk` (`address_id`),
    CONSTRAINT `warehouses_addresses_address_id_fk` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`address_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 8
  DEFAULT CHARSET = latin1;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
