package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Country;
import com.team007.deliver_it.repositories.CountryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.team007.deliver_it.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CountryServiceImplTests {

    @Mock
    CountryRepository mockRepository;

    @InjectMocks
    CountryServiceImpl countryService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        countryService.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnCountry_when_matchExist() {
        Country mockCountry = createMockCountry();
        Mockito.when(mockRepository.getById(mockCountry.getId()))
                .thenReturn(mockCountry);

        Country result = countryService.getById(mockCountry.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCountry.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCountry.getName(), result.getName())
        );
    }
}
