package com.team007.deliver_it.services;

import com.team007.deliver_it.models.DeliveryType;
import com.team007.deliver_it.repositories.DeliveryTypeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class DeliveryTypeServiceImplTests {

    @Mock
    DeliveryTypeRepository mockRepository;

    @InjectMocks
    DeliveryTypeServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById_should_callRepository() {
        DeliveryType deliveryType = new DeliveryType();
        deliveryType.setId(1);
        deliveryType.setDeliveryTypeName("Test");

        service.getById(deliveryType.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getById(deliveryType.getId());
    }
}
