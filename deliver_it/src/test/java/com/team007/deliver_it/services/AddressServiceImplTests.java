package com.team007.deliver_it.services;


import com.team007.deliver_it.models.Address;
import com.team007.deliver_it.repositories.AddressRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.team007.deliver_it.Helpers.createAddress;

@ExtendWith(MockitoExtension.class)
public class AddressServiceImplTests {

    @Mock
    AddressRepository mockRepository;

    @InjectMocks
    AddressServiceImpl service;

    @Test
    void create_should_throw_when_invalidAddress() {
        Address address = new Address();

        Assertions.assertThrows(IllegalArgumentException.class, () -> service.create(address));
    }

    @Test
    void create_should_callRepository_when_validAddress() {
        Address address = createAddress();

        service.create(address);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(address);
    }

    @Test
    void getById_should_callRepository() {
        Address address = createAddress();

        service.getById(address.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getById(address.getId());
    }

    @Test
    void update_should_throw_when_invalidAddress() {
        Address address = new Address();

        Assertions.assertThrows(IllegalArgumentException.class, () -> service.update(address));
    }

    @Test
    void update_should_callRepository_when_validAddress() {
        Address address = createAddress();

        service.update(address);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(address);
    }

    @Test
    void delete_should_callRepository() {
        Address address = createAddress();

        service.delete(address);

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(address);
    }
}
