package com.team007.deliver_it.services;

import com.team007.deliver_it.models.User;
import com.team007.deliver_it.models.UserAddress;
import com.team007.deliver_it.repositories.UserAddressRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.team007.deliver_it.Helpers.*;


@ExtendWith(MockitoExtension.class)
public class UserAddressServiceImplTests {

    @Mock
    UserAddressRepository mockRepository;

    @InjectMocks
    UserAddressServiceImpl service;

    @Test
    void create_should_callRepository() {
        UserAddress userAddress = new UserAddress();

        service.create(userAddress);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(userAddress);
    }

    @Test
    void getByUserId_should_callRepository() {
        User mockUser = createMockCustomer();

        service.getByUserId(mockUser.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getByUserID(mockUser.getId());
    }

    @Test
    void delete_should_callRepository() {
        UserAddress userAddress = new UserAddress();

        service.delete(userAddress);

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(userAddress);
    }
}
