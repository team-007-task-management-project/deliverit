package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Role;
import com.team007.deliver_it.repositories.RoleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.team007.deliver_it.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class RoleServiceImplTests {

    @Mock
    RoleRepository mockRepository;

    @InjectMocks
    RoleServiceImpl roleService;

    @Test
    public void getById_should_returnRole_when_matchExist() {
        Role mockRole = createMockRole("Customer");
        Mockito.when(mockRepository.getById(mockRole.getId()))
                .thenReturn(mockRole);

        Role result = roleService.getById(mockRole.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockRole.getId(), result.getId()),
                () -> Assertions.assertEquals(mockRole.getName(), result.getName())
        );
    }
}
