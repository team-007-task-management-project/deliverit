package com.team007.deliver_it.services;

import com.team007.deliver_it.models.*;
import com.team007.deliver_it.repositories.ShipmentRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.Optional;

import static com.team007.deliver_it.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ShipmentServiceImplTests {

    @Mock
    ShipmentRepository mockRepository;

    @Mock
    StatusService statusService;

    @Mock
    WarehouseService warehouseService;

    @InjectMocks
    ShipmentServiceImpl shipmentService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        shipmentService.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnShipment_when_matchExist() {
        Shipment mockShipment = createMockShipment(1);

        Mockito.when(mockRepository.getById(mockShipment.getId()))
                .thenReturn(mockShipment);

        Shipment result = shipmentService.getById(mockShipment.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockShipment.getId(), result.getId()),
                () -> Assertions.assertEquals(mockShipment.getDepartureDate(), result.getDepartureDate()),
                () -> Assertions.assertEquals(mockShipment.getArrivalDate(), result.getArrivalDate()),
                () -> Assertions.assertEquals(mockShipment.getOriginWarehouse().getId(), result.getOriginWarehouse().getId()),
                () -> Assertions.assertEquals(mockShipment.getDestinationWarehouse().getId(), result.getDestinationWarehouse().getId()),
                () -> Assertions.assertEquals(mockShipment.getStatus().getId(), result.getStatus().getId())
        );
    }

    @Test
    void getByStatus_should_callRepository() {
        Shipment mockShipment = createMockShipment(1);

        Mockito.when(statusService.getById(Mockito.anyInt())).thenReturn(new Status());

        Mockito.when(mockRepository.getByStatus(mockShipment.getStatus().getId()))
                .thenReturn(new ArrayList<>());

        shipmentService.getByStatus(mockShipment.getStatus().getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getByStatus(mockShipment.getStatus().getId());
    }


    @Test
    void getNextShipmentByDestinationWarehouseId_should_callRepository() {
        Shipment mockShipment = createMockShipment(1);

        Mockito.when(warehouseService.getById(Mockito.anyInt())).thenReturn(new Warehouse());

        shipmentService.getNextShipmentByDestinationWarehouseId(mockShipment.getDestinationWarehouse().getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getNextShipmentByDestinationWarehouseId(mockShipment.getDestinationWarehouse().getId(), warehouseService.getById(1));
    }

    @Test
    void filter_should_callRepository() {
        Shipment mockShipment = createMockShipment(1);
        User mockUser = createMockEmployee();

        Mockito.when(mockRepository.filter(Optional.of(mockShipment.getStatus().getId()),
                        Optional.of(mockShipment.getDestinationWarehouse().getId()),
                        Optional.of(mockUser.getId())))
                .thenReturn(new ArrayList<>());

        shipmentService.filter(
                Optional.of(mockShipment.getStatus().getId()),
                Optional.of(mockShipment.getDestinationWarehouse().getId()),
                Optional.of(mockUser.getId()));

        Mockito.verify(mockRepository, Mockito.times(1))
                .filter(Optional.of(mockShipment.getStatus().getId()),
                        Optional.of(mockShipment.getDestinationWarehouse().getId()),
                        Optional.of(mockUser.getId()));
    }

    @Test
    public void create_should_throwException_when_initialStatusIsNotValid() {
        Shipment mockShipment = createMockShipment(1);
        mockShipment.getStatus().setId(2);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> shipmentService.create(mockShipment));
    }

    @Test
    public void create_should_throwException_when_shipmentIsNotValid() {
        Shipment mockShipment = createMockShipment(1);
        mockShipment.getDestinationWarehouse().setId(1);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> shipmentService.create(mockShipment));
    }

    @Test
    public void create_should_callRepository() {
        Shipment mockShipment = createMockShipment(1);

        shipmentService.create(mockShipment);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockShipment);
    }

    @Test
    public void update_should_throwException_when_shipmentIsNotValid() {
        Shipment mockShipment = createMockShipment(1);
        mockShipment.getDestinationWarehouse().setId(1);

        Mockito.when(mockRepository.getById(mockShipment.getId()))
                .thenReturn(mockShipment);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> shipmentService.update(mockShipment));
    }

    @Test
    public void update_should_throwException_when_shipmentHasNoParcelsAndStatusIsNotPreparing() {
        Shipment mockShipment = createMockShipment(1);
        mockShipment.getStatus().setId(2);

        Mockito.when(mockRepository.getById(mockShipment.getId()))
                .thenReturn(mockShipment);

        Mockito.when(mockRepository.hasParcels(mockShipment))
                .thenReturn(false);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> shipmentService.update(mockShipment));
    }

    @Test
    public void update_should_callRepository() {
        Shipment mockShipment = createMockShipment(1);

        Mockito.when(mockRepository.getById(mockShipment.getId()))
                .thenReturn(mockShipment);

        shipmentService.update(mockShipment);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockShipment);
    }

    @Test
    public void delete_should_callRepository() {
        Shipment mockShipment = createMockShipment(1);

        shipmentService.delete(mockShipment.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockShipment.getId());
    }
}
