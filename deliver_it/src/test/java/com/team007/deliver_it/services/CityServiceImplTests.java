package com.team007.deliver_it.services;

import com.team007.deliver_it.models.City;
import com.team007.deliver_it.repositories.CityRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.ArrayList;

import static com.team007.deliver_it.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CityServiceImplTests {

    @Mock
    CityRepository mockRepository;

    @InjectMocks
    CityServiceImpl cityService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        cityService.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnCity_when_matchExist() {
        City mockCity = createMockCity();
        Mockito.when(mockRepository.getById(mockCity.getId()))
                .thenReturn(mockCity);

        City result = cityService.getById(mockCity.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCity.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCity.getCountry().getId(), result.getCountry().getId())
        );
    }
}
