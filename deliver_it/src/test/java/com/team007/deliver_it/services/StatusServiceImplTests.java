package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Address;
import com.team007.deliver_it.models.Status;
import com.team007.deliver_it.repositories.StatusRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.team007.deliver_it.Helpers.createAddress;

@ExtendWith(MockitoExtension.class)
public class StatusServiceImplTests {

    @Mock
    StatusRepository mockRepository;

    @InjectMocks
    StatusServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById_should_callRepository() {
        Status status = new Status();
        status.setStatusName("Test");
        status.setId(1);

        service.getById(status.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getById(status.getId());
    }
}
