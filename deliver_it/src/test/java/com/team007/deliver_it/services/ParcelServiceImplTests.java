package com.team007.deliver_it.services;

import com.team007.deliver_it.exceptions.UnauthorizedOperationException;
import com.team007.deliver_it.models.Parcel;
import com.team007.deliver_it.models.Status;
import com.team007.deliver_it.models.User;
import com.team007.deliver_it.repositories.ParcelRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.team007.deliver_it.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ParcelServiceImplTests {

    @Mock
    ParcelRepository mockRepository;

    @Mock
    StatusService statusService;

    @InjectMocks
    ParcelServiceImpl parcelService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        parcelService.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnParcel_when_matchExist() {
        Parcel mockParcel = createMockParcel();
        Mockito.when(mockRepository.getById(mockParcel.getId()))
                .thenReturn(mockParcel);

        Parcel result = parcelService.getById(mockParcel.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockParcel.getId(), result.getId()),
                () -> Assertions.assertEquals(mockParcel.getUser().getId(), result.getUser().getId()),
                () -> Assertions.assertEquals(mockParcel.getWarehouse().getId(), result.getWarehouse().getId()),
                () -> Assertions.assertEquals(mockParcel.getWeight(), result.getWeight()),
                () -> Assertions.assertEquals(mockParcel.getDeliveryType().getId(), result.getDeliveryType().getId()),
                () -> Assertions.assertEquals(mockParcel.getShipment().getId(), result.getShipment().getId()),
                () -> Assertions.assertEquals(mockParcel.getCategory().getId(), result.getCategory().getId())
        );
    }

    @Test
    void getByStatus_should_callRepository() {
        Parcel mockParcel = createMockParcel();
        User mockUser = createMockCustomer();

        Mockito.when(statusService.getById(Mockito.anyInt())).thenReturn(new Status());

        Mockito.when(mockRepository.getByStatus(mockParcel.getShipment().getStatus().getId(), mockUser.getId()))
                .thenReturn(new ArrayList<>());

        parcelService.getByStatus(mockParcel.getShipment().getStatus().getId(), mockUser.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getByStatus(mockParcel.getShipment().getStatus().getId(), mockUser.getId());
    }

    @Test
    void getStatus_should_throwException_when_userDoesNotOwnParcel() {
        Parcel mockParcel = createMockParcel();
        User mockUser = createMockCustomer();
        mockUser.setId(2);

        Mockito.when(mockRepository.getById(mockParcel.getId()))
                .thenReturn(mockParcel);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.getStatus(mockParcel.getId(), mockUser));
    }

    @Test
    void getStatus_should_callRepository_when_userIsCustomerAndOwnsTheParcel() {
        Parcel mockParcel = createMockParcel();
        User mockUser = createMockCustomer();

        Mockito.when(mockRepository.getById(mockParcel.getId()))
                .thenReturn(mockParcel);
        Mockito.when(mockRepository.getStatus(mockParcel.getId()))
                .thenReturn(new Status());

        parcelService.getStatus(mockParcel.getId(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .getStatus(mockParcel.getId());
    }

    @Test
    void filter_should_callRepository() {
        Parcel mockParcel = createMockParcel();

        Mockito.when(mockRepository.filter(Optional.of(mockParcel.getUser().getId()),
                        Optional.of(mockParcel.getWarehouse().getId()),
                        Optional.of(mockParcel.getCategory().getId()),
                        Optional.of(mockParcel.getWeight()),
                        Optional.of(mockParcel.getWeight()),
                        Optional.of(mockParcel.getShipment().getStatus().getId())))
                .thenReturn(new ArrayList<>());

        parcelService.filter(
                Optional.of(mockParcel.getUser().getId()),
                Optional.of(mockParcel.getWarehouse().getId()),
                Optional.of(mockParcel.getCategory().getId()),
                Optional.of(mockParcel.getWeight()),
                Optional.of(mockParcel.getWeight()),
                Optional.of(mockParcel.getShipment().getStatus().getId()));

        Mockito.verify(mockRepository, Mockito.times(1))
                .filter(Optional.of(mockParcel.getUser().getId()),
                        Optional.of(mockParcel.getWarehouse().getId()),
                        Optional.of(mockParcel.getCategory().getId()),
                        Optional.of(mockParcel.getWeight()),
                        Optional.of(mockParcel.getWeight()),
                        Optional.of(mockParcel.getShipment().getStatus().getId()));
    }

    @Test
    void sort_should_callRepository() {
        Mockito.when(mockRepository.sort(Optional.of("asc"), Optional.of("desc")))
                .thenReturn(new ArrayList<>());

        parcelService.sort(
                Optional.of("asc"), Optional.of("desc"));

        Mockito.verify(mockRepository, Mockito.times(1))
                .sort(Optional.of("asc"), Optional.of("desc"));
    }

    @Test
    public void create_should_throwException_when_deliveryTypeIsNotValid() {
        Parcel mockParcel = createMockParcel();
        mockParcel.getDeliveryType().setId(2);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> parcelService.create(mockParcel));
    }

    @Test
    public void create_should_throwException_when_parcelOwnerIsNotCustomer() {
        Parcel mockParcel = createMockParcel();
        mockParcel.getUser().getRole().setName("Employee");

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> parcelService.create(mockParcel));
    }

    @Test
    public void create_should_callRepository() {
        Parcel mockParcel = createMockParcel();

        parcelService.create(mockParcel);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockParcel);
    }

    @Test
    public void customerUpdate_should_throwException_when_userIsNotCustomer() {
        Parcel mockParcel = createMockParcel();
        User mockUser = createMockEmployee();
        mockUser.setEmail("mockemployeee@user.com");

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.customerUpdate(mockUser, mockParcel));
    }

    @Test
    public void customerUpdate_should_throwException_when_shipmentStatusIsAlreadyCompleted() {
        Parcel mockParcel = createMockParcel();
        User mockUser = createMockCustomer();
        mockParcel.getShipment().getStatus().setId(3);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> parcelService.customerUpdate(mockUser, mockParcel));
    }

    @Test
    public void customerUpdate_should_throwException_when_someoneElseTriesToUpdateParcel() {
        Parcel mockParcel = createMockParcel();
        User mockUser = createMockCustomer();
        mockUser.setEmail("anothermock@user.com");

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> parcelService.customerUpdate(mockUser, mockParcel));
    }

    @Test
    public void customerUpdate_should_callRepository_when_userIsCustomerAndOwnerOfParcel() {
        Parcel mockParcel = createMockParcel();
        User mockUser = createMockCustomer();

        parcelService.customerUpdate(mockUser, mockParcel);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockParcel);
    }

    @Test
    public void employeeUpdate_should_throwException_when_statusOfParcelIsAlreadyCompleted() {
        Parcel mockParcel = createMockParcel();
        mockParcel.getShipment().getStatus().setId(3);

        Mockito.when(mockRepository.getById(mockParcel.getId()))
                .thenReturn(mockParcel);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> parcelService.employeeUpdate(mockParcel));
    }

    @Test
    public void employeeUpdate_should_throwException_when_parcelOwnerIsNotCustomer() {
        Parcel mockParcel = createMockParcel();
        mockParcel.getUser().getRole().setName("Employee");

        Mockito.when(mockRepository.getById(mockParcel.getId()))
                .thenReturn(mockParcel);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> parcelService.employeeUpdate(mockParcel));
    }

    @Test
    public void employeeUpdate_should_callRepository() {
        Parcel mockParcel = createMockParcel();

        Mockito.when(mockRepository.getById(mockParcel.getId()))
                .thenReturn(mockParcel);

        parcelService.employeeUpdate(mockParcel);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockParcel);
    }

    @Test
    public void delete_should_callRepository() {
        Parcel mockParcel = createMockParcel();

        parcelService.delete(mockParcel.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockParcel.getId());
    }
}
