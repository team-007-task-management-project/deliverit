package com.team007.deliver_it.services;

import com.team007.deliver_it.exceptions.DuplicateEntityException;
import com.team007.deliver_it.exceptions.UnauthorizedOperationException;
import com.team007.deliver_it.mappers.AddressMapper;
import com.team007.deliver_it.models.User;
import com.team007.deliver_it.models.UserAddress;
import com.team007.deliver_it.models.dtos.UserDto;
import com.team007.deliver_it.repositories.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.Optional;
import static com.team007.deliver_it.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockRepository;

    @Mock
    UserAddressService userAddressService;

    @Mock
    AddressService addressService;

    @Mock
    AddressMapper addressMapper;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        userService.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void getAllCustomers_should_callRepository() {
        Mockito.when(mockRepository.getAllCustomers())
                .thenReturn(new ArrayList<>());

        userService.getAllCustomers();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllCustomers();
    }

    @Test
    void getAllEmployees_should_callRepository() {
        Mockito.when(mockRepository.getAllEmployees())
                .thenReturn(new ArrayList<>());

        userService.getAllEmployees();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllEmployees();
    }

    @Test
    public void getById_should_returnUser_when_matchExist() {
        User mockUser = createMockCustomer();

        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        User result = userService.getById(mockUser.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), result.getLastName())
        );
    }

    @Test
    public void getByEmail_should_returnUser_when_matchExist() {
        User mockUser = createMockCustomer();

        Mockito.when(mockRepository.getByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        User result = userService.getByEmail(mockUser.getEmail());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), result.getLastName())
        );
    }

    @Test
    void getCustomerIncomingParcels_should_throw_when_userIsNotOwnerOfParcelAndCustomerOrEmployee() {
        User mockUser = createMockCustomer();
        User mockUser2 = createMockCustomer();
        mockUser2.setEmail("mock2@user.com");
        mockUser2.setId(2);

        Mockito.when(mockRepository.getById(mockUser2.getId()))
                .thenReturn(mockUser2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.getCustomerIncomingParcels(mockUser, mockUser2.getId()));
    }

    @Test
    void getCustomerIncomingParcels_should_callRepository() {
        User mockUser = createMockCustomer();

        Mockito.when(mockRepository.getCustomerIncomingParcels(mockUser.getId()))
                .thenReturn(new ArrayList<>());

        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        userService.getCustomerIncomingParcels(mockUser, mockUser.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getCustomerIncomingParcels(mockUser.getId());
    }

    @Test
    void filter_should_callRepository() {
        User mockUser = createMockEmployee();

        Optional<String> firstName = Optional.ofNullable(mockUser.getFirstName());
        Optional<String> lastName = Optional.ofNullable(mockUser.getLastName());
        Optional<String> email = Optional.ofNullable(mockUser.getEmail());

        Mockito.when(mockRepository.filter(firstName, lastName, email))
                .thenReturn(new ArrayList<>());

        userService.filter(firstName, lastName, email);

        Mockito.verify(mockRepository, Mockito.times(1))
                .filter(firstName, lastName, email);
    }

    @Test
    void search_should_callRepository() {
        User mockUser = createMockEmployee();

        Optional<String> keyWord = Optional.ofNullable(mockUser.getFirstName());

        Mockito.when(mockRepository.search(keyWord))
                .thenReturn(new ArrayList<>());

        userService.search(keyWord);

        Mockito.verify(mockRepository, Mockito.times(1))
                .search(keyWord);
    }

    @Test
    void createCustomer_should_throw_when_emailExists() {
        User mockUser = createMockCustomer();
        UserDto mockUserDto = createMockUserDto("Street 1", 1);
        User mockUser2 = createMockCustomer();
        mockUser2.setId(2);

        Mockito.when(userService.getByEmail(mockUser.getEmail())).thenReturn(mockUser);

        Assertions.assertThrows(DuplicateEntityException.class, () -> userService.createCustomer(mockUser2, mockUserDto));
    }

    @Test
    void createCustomer_should_callRepository() {
        User mockUser = createMockCustomer();
        UserDto mockUserDto = createMockUserDto("Street 1", 1);

        Mockito.when(userService.getByEmail(mockUser.getEmail())).thenReturn(mockUser);

        userService.createCustomer(mockUser, mockUserDto);

        Mockito.verify(mockRepository, Mockito.times(1))
                .createCustomer(mockUser);
    }

    @Test
    void createEmployee_should_throw_when_emailExists() {
        User mockUser = createMockEmployee();
        UserDto mockUserDto = createMockUserDto("Street 1", 1);
        User mockUser2 = createMockEmployee();
        mockUser2.setId(2);

        Mockito.when(userService.getByEmail(mockUser.getEmail())).thenReturn(mockUser);

        Assertions.assertThrows(DuplicateEntityException.class, () -> userService.createEmployee(mockUser2, mockUserDto));
    }

    @Test
    void createEmployee_should_callRepository() {
        User mockUser = createMockEmployee();
        UserDto mockUserDto = createMockUserDto("Street 1", 1);

        Mockito.when(userService.getByEmail(mockUser.getEmail())).thenReturn(mockUser);

        userService.createEmployee(mockUser, mockUserDto);

        Mockito.verify(mockRepository, Mockito.times(1))
                .createEmployee(mockUser);
    }

    @Test
    void update_should_throw_when_userHasNoPermission() {
        User mockUser = createMockEmployee();
        UserDto mockUserDto = createMockUserDto("Street 1", 1);
        User mockUser2 = createMockCustomer();
        mockUser2.setEmail("mock2@user.com");
        mockUser2.setId(2);

        Mockito.when(userService.getByEmail(mockUser.getEmail())).thenReturn(mockUser);

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> userService.update(mockUser, mockUser2, mockUserDto));
    }

    @Test
    void update_should_throw_when_emailExists() {
        User mockUser = createMockEmployee();
        UserDto mockUserDto = createMockUserDto("Street 1", 1);
        User mockUser2 = createMockEmployee();
        mockUser2.setId(2);

        Mockito.when(userService.getByEmail(mockUser.getEmail())).thenReturn(mockUser);

        Assertions.assertThrows(DuplicateEntityException.class, () -> userService.update(mockUser2, mockUser, mockUserDto));
    }

    @Test
    void update_should_callRepository() {
        User mockUser = createMockEmployee();
        mockUser.setEmail("newMock@user.com");
        UserDto mockUserDto = createMockUserDto("Street 1", 1);

        Mockito.when(userService.getByEmail(mockUser.getEmail())).thenReturn(mockUser);

        userService.update(mockUser, mockUser, mockUserDto);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    void delete_should_throw_when_userHasNoPermission() {
        User mockUser = createMockEmployee();
        User mockUser2 = createMockCustomer();
        mockUser2.setEmail("mock2@user.com");
        mockUser2.setId(2);

        Mockito.when(userService.getById(mockUser.getId())).thenReturn(mockUser);
        Mockito.when(userAddressService.getByUserId(mockUser.getId())).thenReturn(new UserAddress());

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> userService.delete(mockUser.getId(), mockUser2));
    }

    @Test
    void delete_should_callRepository() {
        User mockUser = createMockEmployee();

        Mockito.when(userService.getById(mockUser.getId())).thenReturn(mockUser);
        Mockito.when(userAddressService.getByUserId(mockUser.getId())).thenReturn(new UserAddress());

        userService.delete(mockUser.getId(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockUser.getId());
    }
}
