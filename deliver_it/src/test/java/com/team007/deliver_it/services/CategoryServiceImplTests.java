package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Category;
import com.team007.deliver_it.repositories.CategoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.ArrayList;

import static com.team007.deliver_it.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceImplTests {

    @Mock
    CategoryRepository mockRepository;

    @InjectMocks
    CategoryServiceImpl categoryService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        categoryService.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnCategory_when_matchExist() {
        Category mockCategory = createMockCategory();
        Mockito.when(mockRepository.getById(mockCategory.getId()))
                .thenReturn(mockCategory);

        Category result = categoryService.getById(mockCategory.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCategory.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCategory.getCategoryName(), result.getCategoryName())
        );
    }
}
