package com.team007.deliver_it.services;

import com.team007.deliver_it.exceptions.UnauthorizedOperationException;
import com.team007.deliver_it.models.User;
import com.team007.deliver_it.models.Warehouse;
import com.team007.deliver_it.repositories.WarehouseRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import static com.team007.deliver_it.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class WarehouseServiceImplTests {

    @Mock
    WarehouseRepository mockRepository;

    @Mock
    AddressServiceImpl mockAddressService;

    @InjectMocks
    WarehouseServiceImpl warehouseService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        warehouseService.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnWarehouse_when_matchExist() {
        Warehouse mockWarehouse = createMockWarehouse(1);
        Mockito.when(mockRepository.getById(mockWarehouse.getId()))
                .thenReturn(mockWarehouse);

        Warehouse result = warehouseService.getById(mockWarehouse.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockWarehouse.getId(), result.getId()),
                () -> Assertions.assertEquals(mockWarehouse.getAddress().getId(), result.getAddress().getId())
        );
    }

    @Test
    public void create_should_callRepository() {
        Warehouse mockWarehouse = createMockWarehouse(1);

        warehouseService.create(mockWarehouse, mockWarehouse.getAddress());

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockWarehouse);
    }

    @Test
    public void update_should_callRepository() {
        Warehouse mockWarehouse = createMockWarehouse(1);

        warehouseService.update(mockWarehouse, mockWarehouse.getAddress());

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockWarehouse);
    }

    @Test
    public void delete_should_callRepository() {
        Warehouse mockWarehouse = createMockWarehouse(1);

        Mockito.when(mockRepository.getById(mockWarehouse.getId())).thenReturn(mockWarehouse);

        warehouseService.delete(mockWarehouse.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockWarehouse.getId());
    }

}
