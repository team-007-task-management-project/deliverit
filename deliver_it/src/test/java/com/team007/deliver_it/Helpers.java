package com.team007.deliver_it;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.team007.deliver_it.models.*;
import com.team007.deliver_it.models.dtos.UserDto;

import java.time.LocalDate;

public class Helpers {

    public static User createMockCustomer() {
        return createMockUser("Customer");
    }

    public static User createMockEmployee() {
        return createMockUser("Employee");
    }

    public static UserDto createMockUserDto(String streetName, int cityId) {
        return createMockDto(streetName, cityId);
    }

    public static Address createAddress() {
        return createMockAddress();
    }

    private static User createMockUser(String role) {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("mock@user.com");
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setRole(createMockRole(role));
        return mockUser;
    }

    private static UserDto createMockDto(String streetName, int cityId) {
        UserDto userDto = new UserDto();
        userDto.setFirstName("MockFirstName");
        userDto.setLastName("MockLastName");
        userDto.setEmail("mock@user.com");
        userDto.setStreetName(streetName);
        userDto.setCityId(cityId);
        return userDto;
    }

    public static Role createMockRole(String role) {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setName(role);
        return mockRole;
    }

    public static Warehouse createMockWarehouse(int warehouseId) {
        var mockWarehouse = new Warehouse();
        mockWarehouse.setId(warehouseId);
        mockWarehouse.setAddress(createMockAddress());
        return mockWarehouse;
    }

    public static Address createMockAddress() {
        var mockAddress = new Address();
        mockAddress.setId(1);
        mockAddress.setStreetName("Street name 1");
        mockAddress.setCity(createMockCity());
        return mockAddress;
    }

    public static City createMockCity() {
        var mockCity = new City();
        mockCity.setId(1);
        mockCity.setName("Sofia");
        mockCity.setCountry(createMockCountry());
        return mockCity;
    }

    public static Country createMockCountry() {
        var mockCountry = new Country();
        mockCountry.setId(1);
        mockCountry.setName("Bulgaria");
        return mockCountry;
    }

    public static Category createMockCategory() {
        var mockCategory = new Category();
        mockCategory.setId(1);
        mockCategory.setCategoryName("Outdoor");
        return mockCategory;
    }

    public static DeliveryType createMockDeliveryType() {
        var mockDeliveryType = new DeliveryType();
        mockDeliveryType.setId(1);
        mockDeliveryType.setDeliveryTypeName("Pick up from warehouse");
        return mockDeliveryType;
    }

    public static Status createMockStatus() {
        var mockStatus = new Status();
        mockStatus.setId(1);
        mockStatus.setStatusName("Preparing");
        return mockStatus;
    }

    public static Shipment createMockShipment(int warehouseId) {
        var mockShipment = new Shipment();
        mockShipment.setId(1);
        mockShipment.setOriginWarehouse(createMockWarehouse(warehouseId));
        mockShipment.setDestinationWarehouse(createMockWarehouse(warehouseId + 1));
        mockShipment.setDepartureDate(LocalDate.now());
        mockShipment.setArrivalDate(LocalDate.now());
        mockShipment.setStatus(createMockStatus());

        return mockShipment;
    }

    public static Parcel createMockParcel() {
        var mockParcel = new Parcel();
        mockParcel.setId(1);
        mockParcel.setUser(createMockCustomer());
        mockParcel.setWarehouse(createMockWarehouse(1));
        mockParcel.setWeight(25);
        mockParcel.setCategory(createMockCategory());
        mockParcel.setShipment(createMockShipment(1));
        mockParcel.setDeliveryType(createMockDeliveryType());
        return mockParcel;
    }

    /**
     * Accepts an object and returns the stringified object.
     * Useful when you need to pass a body to a HTTP request.
     */
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
