package com.team007.deliver_it.controllers.rest;

import com.team007.deliver_it.controllers.AuthenticationHelper;
import com.team007.deliver_it.exceptions.*;
import com.team007.deliver_it.mappers.UserMapper;
import com.team007.deliver_it.models.*;
import com.team007.deliver_it.models.dtos.UserDto;
import com.team007.deliver_it.services.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static com.team007.deliver_it.models.User.checkIfEmployee;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService service;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService service, UserMapper userMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ApiOperation(value = "Returns all users.")
    @GetMapping
    public List<User> getAll(@RequestHeader HttpHeaders headers) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            return service.getAll();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns all customers.")
    @GetMapping("/customers")
    public List<User> getAllCustomers() {
        return service.getAllCustomers();
    }

    @ApiOperation(value = "Returns all employees.")
    @GetMapping("/employees")
    public List<User> getAllEmployees(@RequestHeader HttpHeaders headers) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            return service.getAllEmployees();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns a user by a given id.")
    @GetMapping("/{id}")
    public User getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns all incoming parcels of a given customer.")
    @GetMapping("/customers/{customerId}/incoming-parcels")
    public List<Parcel> getCustomerIncomingParcels(@RequestHeader HttpHeaders headers, @PathVariable int customerId) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);

            return service.getCustomerIncomingParcels(authenticatedUser, customerId);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns all users filtered by firstName, lastName and email.")
    @GetMapping("/filter")
    public List<User> filter(@RequestHeader HttpHeaders headers,
                             @RequestParam(required = false) Optional<String> firstName,
                             @RequestParam(required = false) Optional<String> lastName,
                             @RequestParam(required = false) Optional<String> email) {
        try {
            authenticationHelper.tryGetUser(headers);

            return service.filter(firstName, lastName, email);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns all users matching a given value for firstName, lastName and/or email.")
    @GetMapping("/search")
    public List<User> search(@RequestHeader HttpHeaders headers,
                             @RequestParam(required = false) Optional<String> keyWord) {
        try {
            authenticationHelper.tryGetUser(headers);

            return service.search(keyWord);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Creates a customer.")
    @PostMapping("/customer")
    public User createCustomer(@Valid @RequestBody UserDto userDto) {
        try {
            User user = userMapper.fromUserDtoToCustomerObject(userDto);

            service.createCustomer(user, userDto);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiOperation(value = "Creates an employee.")
    @PostMapping("/employee")
    public User createEmployee(@RequestHeader HttpHeaders headers, @Valid @RequestBody UserDto userDto) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            User user = userMapper.fromUserDtoToEmployeeObject(userDto);
            service.createEmployee(user, userDto);

            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Updates a user.")
    @PutMapping("/{id}")
    public User update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody UserDto userDto) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);

            User user = userMapper.fromUserDtoToUserObject(userDto, id);
            service.update(user, authenticatedUser, userDto);

            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Deletes a user.")
    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);

            service.delete(id, authenticatedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
