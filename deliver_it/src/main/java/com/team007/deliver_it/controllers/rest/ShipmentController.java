package com.team007.deliver_it.controllers.rest;

import com.team007.deliver_it.controllers.AuthenticationHelper;
import com.team007.deliver_it.exceptions.EntityNotFoundException;
import com.team007.deliver_it.exceptions.UnauthorizedOperationException;
import com.team007.deliver_it.mappers.ShipmentMapper;
import com.team007.deliver_it.models.Shipment;
import com.team007.deliver_it.models.User;
import com.team007.deliver_it.models.dtos.ShipmentDto;
import com.team007.deliver_it.services.ShipmentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static com.team007.deliver_it.models.User.checkIfEmployee;

@RestController
@RequestMapping("/api/shipments")
public class ShipmentController {

    private final ShipmentService service;
    private final AuthenticationHelper authenticationHelper;
    private final ShipmentMapper shipmentMapper;

    @Autowired
    public ShipmentController(ShipmentService service, AuthenticationHelper authenticationHelper, ShipmentMapper shipmentMapper) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.shipmentMapper = shipmentMapper;
    }

    @ApiOperation(value = "Returns all shipments.")
    @GetMapping
    public List<Shipment> getAll(@RequestHeader HttpHeaders headers) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            return service.getAll();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns a shipment by a given id.")
    @GetMapping("/{id}")
    public Shipment getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns all shipments based on a given status.")
    @GetMapping("/status")
    public List<Shipment> getByStatus(@RequestHeader HttpHeaders headers, @RequestParam int statusId) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            return service.getByStatus(statusId);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns the next shipment to arrive for a given warehouse.")
    @GetMapping("/next-shipment")
    public Shipment getNextShipment(@RequestHeader HttpHeaders headers, @RequestParam int warehouseId) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            return service.getNextShipmentByDestinationWarehouseId(warehouseId);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns all shipments filtered by warehouseId and/or userId.")
    @GetMapping("/filter")
    public List<Shipment> filter(@RequestHeader HttpHeaders headers,
                                 @RequestParam(required = false) Optional<Integer> statusId,
                                 @RequestParam(required = false) Optional<Integer> warehouseId,
                                 @RequestParam(required = false) Optional<Integer> userId) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            return service.filter(statusId, warehouseId, userId);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Creates a shipment.")
    @PostMapping
    public Shipment create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ShipmentDto shipmentDto) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            Shipment shipment = shipmentMapper.fromShipmentDtoToShipmentObject(shipmentDto);
            service.create(shipment);
            return shipment;
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation(value = "Updates a shipment.")
    @PutMapping("/{id}")
    public Shipment update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody ShipmentDto shipmentDto) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            Shipment shipment = shipmentMapper.fromShipmentDtoToShipmentObject(shipmentDto, id);

            service.update(shipment);
            return shipment;
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @ApiOperation(value = "Deletes a shipment.")
    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            service.delete(id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
