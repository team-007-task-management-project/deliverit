package com.team007.deliver_it.controllers.rest;

import com.team007.deliver_it.controllers.AuthenticationHelper;
import com.team007.deliver_it.exceptions.DuplicateEntityException;
import com.team007.deliver_it.exceptions.EntityNotFoundException;
import com.team007.deliver_it.exceptions.UnauthorizedOperationException;
import com.team007.deliver_it.mappers.AddressMapper;
import com.team007.deliver_it.models.Address;
import com.team007.deliver_it.models.User;
import com.team007.deliver_it.models.Warehouse;
import com.team007.deliver_it.mappers.WarehouseMapper;
import com.team007.deliver_it.models.dtos.WarehouseDto;
import com.team007.deliver_it.services.WarehouseService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.util.List;

import static com.team007.deliver_it.models.User.checkIfEmployee;

@RestController
@RequestMapping("/api/warehouses")
public class WarehouseController {
    private final WarehouseService warehouseService;
    private final WarehouseMapper warehouseMapper;
    private final AddressMapper addressMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public WarehouseController(WarehouseService warehouseService, WarehouseMapper warehouseMapper, AddressMapper addressMapper, AuthenticationHelper authenticationHelper) {
        this.warehouseService = warehouseService;
        this.warehouseMapper = warehouseMapper;
        this.addressMapper = addressMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ApiOperation(value = "Returns all warehouses.")
    @GetMapping
    public List<Warehouse> getAll() {
        return warehouseService.getAll();
    }

    @ApiOperation(value = "Returns a warehouse by a given id.")
    @GetMapping("/{id}")
    public Warehouse getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            return warehouseService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Creates a warehouse.")
    @PostMapping
    public Warehouse create(@RequestHeader HttpHeaders headers, @Valid @RequestBody WarehouseDto warehouseDto) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            Address address = addressMapper.fromWarehouseDtoToAddressObject(warehouseDto);
            Warehouse warehouse = warehouseMapper.fromWarehouseDtoToWarehouseObject(address);
            warehouseService.create(warehouse, address);
            return warehouse;

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @ApiOperation(value = "Updates a warehouse.")
    @PutMapping("/{id}")
    public Warehouse update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody WarehouseDto warehouseDto) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            Warehouse warehouse = warehouseMapper.fromWarehouseDtoToWarehouseObject(warehouseDto, id);
            warehouseService.update(warehouse, warehouse.getAddress());
            return warehouse;
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Deletes a warehouse.")
    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            warehouseService.delete(id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
