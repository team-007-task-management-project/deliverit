package com.team007.deliver_it.controllers.rest;

import com.team007.deliver_it.exceptions.EntityNotFoundException;
import com.team007.deliver_it.models.City;
import com.team007.deliver_it.models.dtos.CityDto;
import com.team007.deliver_it.services.CityService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;

@RestController
@RequestMapping("/api/cities")
public class CityController {

    private final CityService service;

    @Autowired
    public CityController(CityService service) {
        this.service = service;
    }

    @ApiOperation(value = "Returns all cities.")
    @GetMapping
    public List<City> getAll() {
        return service.getAll();
    }

    @ApiOperation(value = "Returns a city based on a given id.")
    @GetMapping("/{id}")
    public CityDto getById(@PathVariable int id) {
        try {
            City city = service.getById(id);
            return new CityDto(city);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
