package com.team007.deliver_it.controllers.mvc;

import com.team007.deliver_it.controllers.AuthenticationHelper;
import com.team007.deliver_it.models.Shipment;
import com.team007.deliver_it.models.Warehouse;
import com.team007.deliver_it.services.ShipmentService;
import com.team007.deliver_it.services.UserService;
import com.team007.deliver_it.services.WarehouseService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomeMvcController {
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final WarehouseService warehouseService;
    private final ShipmentService shipmentService;

    public HomeMvcController(UserService userService, AuthenticationHelper authenticationHelper, WarehouseService warehouseService, ShipmentService shipmentService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.warehouseService = warehouseService;
        this.shipmentService = shipmentService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isCustomer")
    public boolean isCustomer(HttpSession session) {
        return (session.getAttribute("currentUser") != null && authenticationHelper.tryGetUser(session).getRole().getName().equals("Customer"));
    }

    @ModelAttribute("isEmployee")
    public boolean isEmployee(HttpSession session) {
        return (session.getAttribute("currentUser") != null && authenticationHelper.tryGetUser(session).getRole().getName().equals("Employee"));
    }

    @ModelAttribute("customers")
    public int customersCount() {
        return userService.getAllCustomers().size();
    }

    @ModelAttribute("warehouses")
    public List<Warehouse> populateWarehouses() {
        if (warehouseService.getAll().size() > 3) {
            return warehouseService.getAll().subList(0, 3);
        } else {
            return warehouseService.getAll();
        }
    }

    @ModelAttribute("shipments")
    public List<Shipment> populateShipments() {
        if (shipmentService.getAll().size() > 3) {
            return shipmentService.getAll().subList(0, 3);
        } else {
            return shipmentService.getAll();
        }
    }

    @GetMapping
    public String showHomePage() {
        return "index";
    }
}
