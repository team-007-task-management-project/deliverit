package com.team007.deliver_it.controllers.mvc;

import com.team007.deliver_it.controllers.AuthenticationHelper;
import com.team007.deliver_it.exceptions.AuthenticationFailureException;
import com.team007.deliver_it.exceptions.EntityNotFoundException;
import com.team007.deliver_it.exceptions.UnauthorizedOperationException;
import com.team007.deliver_it.mappers.ParcelMapper;
import com.team007.deliver_it.models.*;
import com.team007.deliver_it.models.dtos.FilterDto;
import com.team007.deliver_it.models.dtos.ParcelDto;
import com.team007.deliver_it.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static com.team007.deliver_it.models.User.checkIfEmployee;

@Controller
@RequestMapping("/parcels")
public class ParcelMvcController {

    private final static int FILTER_STATUS_NOT_SPECIFIED = 0;
    private final static int CUSTOMER_ROLE_ID = 1;

    private final ParcelService parcelService;
    private final ParcelMapper parcelMapper;
    private final DeliveryTypeService deliveryTypeService;
    private final CategoryService categoryService;
    private final AuthenticationHelper authenticationHelper;
    private final StatusService statusService;
    private final WarehouseService warehouseService;

    @Autowired
    public ParcelMvcController(ParcelService parcelService, ParcelMapper parcelMapper, DeliveryTypeService deliveryTypeService, CategoryService categoryService, AuthenticationHelper authenticationHelper, StatusService statusService, WarehouseService warehouseService) {
        this.parcelService = parcelService;
        this.parcelMapper = parcelMapper;
        this.deliveryTypeService = deliveryTypeService;
        this.categoryService = categoryService;
        this.authenticationHelper = authenticationHelper;
        this.statusService = statusService;
        this.warehouseService = warehouseService;
    }

    @ModelAttribute("isCustomer")
    public boolean isCustomer(HttpSession session) {
        return (session.getAttribute("currentUser") != null && authenticationHelper.tryGetUser(session).getRole().getName().equals("Customer"));
    }

    @ModelAttribute("isEmployee")
    public boolean isEmployee(HttpSession session) {
        return (session.getAttribute("currentUser") != null && authenticationHelper.tryGetUser(session).getRole().getName().equals("Employee"));
    }

    @ModelAttribute("deliveryTypes")
    public List<DeliveryType> populateDeliveryTypes() {
        return deliveryTypeService.getAll();
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("statuses")
    public List<Status> populateStatuses() {
        return statusService.getAll();
    }

    @ModelAttribute("warehouses")
    public List<Warehouse> populateWarehouses() {
        return warehouseService.getAll();
    }

    @GetMapping
    public String showAllParcels(Model model) {
        try {
            model.addAttribute("parcels", parcelService.getAll());
            model.addAttribute("filterDto", new FilterDto());

            return "parcels-employee";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/customer")
    public String showUserParcels(Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        model.addAttribute("parcels", parcelService.getAllForUser(user));
        model.addAttribute("filterDto", new FilterDto());

        return "parcels-customer";
    }

    @GetMapping("/{id}")
    public String showSingleParcel(@PathVariable int id, Model model) {
        try {
            Parcel parcel = parcelService.getById(id);
            boolean isCompleted;
            if (parcel.getShipment() == null) {
                isCompleted = false;
            } else {
                isCompleted = parcel.getShipment().getStatus().getStatusName().equals("Completed");
            }
            model.addAttribute("parcel", parcel);
            model.addAttribute("isCompleted", isCompleted);

            return "parcel";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "parcels-employee";
        }
    }

    @GetMapping("/search")
    public String showSingleParcel(FilterDto filterDto, Model model) {
            if (filterDto.getId() == null) {
                return "parcels-employee";
            }
            return showSingleParcel(filterDto.getId(), model);
    }

    @GetMapping("/filter")
    public String filter(@ModelAttribute FilterDto filterDto, HttpSession session, Model model) {
        User user = authenticationHelper.tryGetUser(session);
        if (isCustomer(user)) {
            return customerFilter(filterDto, model, user);
        }
        return employeeFilter(filterDto, model);
    }

    @GetMapping("/new")
    public String showNewParcelPage(Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        model.addAttribute("parcel", new ParcelDto());
        try {
            checkIfEmployee(user);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
        return "parcel-new";
    }

    @PostMapping("/new")
    public String createParcel(@Valid @ModelAttribute("parcel") ParcelDto parcelDto,
                               BindingResult errors,
                               Model model) {
        if (errors.hasErrors()) {
            return "parcel-new";
        }

        try {
            Parcel parcel = parcelMapper.fromParcelDtoToParcelObject(parcelDto);
            parcelService.create(parcel);

            return "redirect:/parcels";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "parcel-new";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditParcelPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Parcel parcel = parcelService.getById(id);
            ParcelDto parcelDto = parcelMapper.toDto(parcel);
            DeliveryType deliveryType = parcel.getDeliveryType();
            model.addAttribute("parcel", parcelDto);
            model.addAttribute("deliveryType", deliveryType);

            if (isCustomer(user)) {
                return "parcel-update-customer";
            }

            return "parcel-update-employee";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateParcelEmployee(@PathVariable int id,
                                       @Valid @ModelAttribute("parcel") ParcelDto parcelDto,
                                       BindingResult errors,
                                       Model model,
                                       HttpSession session) {
        User authUser;
        try {
            authUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "parcel-update-employee";
        }

        try {
            Parcel parcel = parcelMapper.fromParcelDtoToParcelObject(parcelDto, id);
            parcelService.employeeUpdate(parcel);
            return "redirect:/parcels";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update/customer")
    public String updateParcelCustomer(@PathVariable int id,
                                       @ModelAttribute("deliveryType") DeliveryType deliveryType,
                                       HttpSession session) {
        User authUser;
        try {
            authUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        Parcel parcel = parcelMapper.updateDeliveryTypeInParcel(deliveryType.getId(), id);
        parcelService.customerUpdate(authUser, parcel);
        return String.format("redirect:/parcels/%s", parcel.getId());
    }

    private String customerFilter(FilterDto filterDto, Model model, User user) {
        if (hasStatus(filterDto)) {
            model.addAttribute("parcels", parcelService.getAllForUser(user));
            return "parcels-customer";
        }
        var filtered = parcelService.getByStatus(filterDto.getStatusId(), user.getId());
        model.addAttribute("parcels", filtered);

        return "parcels-customer";
    }

    private String employeeFilter(FilterDto filterDto, Model model) {
        var filtered = parcelService.filter(
                Optional.ofNullable(filterDto.getUserId()),
                Optional.ofNullable(filterDto.getWarehouseId()),
                Optional.ofNullable(filterDto.getCategoryId()),
                Optional.ofNullable(filterDto.getMinWeight()),
                Optional.ofNullable(filterDto.getMaxWeight()),
                Optional.ofNullable(filterDto.getStatusId()));

        model.addAttribute("parcels", filtered);

        return "parcels-employee";
    }

    private boolean isCustomer(User user) {
        return user.getRole().getId() == CUSTOMER_ROLE_ID;
    }

    private boolean hasStatus(FilterDto filterDto) {
        return filterDto.getStatusId() == FILTER_STATUS_NOT_SPECIFIED;
    }
}
