package com.team007.deliver_it.controllers.mvc;

import com.team007.deliver_it.controllers.AuthenticationHelper;
import com.team007.deliver_it.exceptions.EntityNotFoundException;
import com.team007.deliver_it.mappers.AddressMapper;
import com.team007.deliver_it.mappers.WarehouseMapper;
import com.team007.deliver_it.models.Address;
import com.team007.deliver_it.models.City;
import com.team007.deliver_it.models.Warehouse;
import com.team007.deliver_it.models.dtos.WarehouseDto;
import com.team007.deliver_it.services.CityService;
import com.team007.deliver_it.services.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/warehouses")
public class WarehouseMvcController {

    private final WarehouseService warehouseService;
    private final WarehouseMapper warehouseMapper;
    private final AddressMapper addressMapper;
    private final CityService cityService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public WarehouseMvcController(WarehouseService warehouseService, WarehouseMapper warehouseMapper, AddressMapper addressMapper, CityService cityService, AuthenticationHelper authenticationHelper) {
        this.warehouseService = warehouseService;
        this.warehouseMapper = warehouseMapper;
        this.addressMapper = addressMapper;
        this.cityService = cityService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("cities")
    public List<City> populateCities() {
        return cityService.getAll();
    }

    @ModelAttribute("isCustomer")
    public boolean isCustomer(HttpSession session) {
        return (session.getAttribute("currentUser") != null && authenticationHelper.tryGetUser(session).getRole().getName().equals("Customer"));
    }

    @ModelAttribute("isEmployee")
    public boolean isEmployee(HttpSession session) {
        return (session.getAttribute("currentUser") != null && authenticationHelper.tryGetUser(session).getRole().getName().equals("Employee"));
    }

    @GetMapping
    public String showAllWarehouses(Model model) {
        model.addAttribute("warehouses", warehouseService.getAll());
        return "warehouses";
    }

    @GetMapping("/{id}")
    public String showSingleWarehouse(@PathVariable int id, Model model) {
        try {
            Warehouse warehouse = warehouseService.getById(id);
            model.addAttribute("warehouse", warehouse);
            return "warehouse";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showNewWarehousePage(Model model) {
        model.addAttribute("warehouse", new WarehouseDto());
        return "warehouse-new";
    }

    @PostMapping("/new")
    public String createWarehouse(@Valid @ModelAttribute("warehouse") WarehouseDto warehouseDto, BindingResult errors, Model model) {
        if (errors.hasErrors()) {
            return "warehouse-new";
        }

        try {
            Address address = addressMapper.fromWarehouseDtoToAddressObject(warehouseDto);
            Warehouse warehouse = warehouseMapper.fromWarehouseDtoToWarehouseObject(address);
            warehouseService.create(warehouse, address);

            return "redirect:/warehouses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditWarehousePage(@PathVariable int id, Model model) {
        try {
            Warehouse warehouse = warehouseService.getById(id);
            WarehouseDto warehouseDto = warehouseMapper.toDto(warehouse);
            model.addAttribute("warehouse", warehouseDto);
            return "warehouse-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateWarehouse(@PathVariable int id,
                                  @Valid @ModelAttribute("warehouse") WarehouseDto warehouseDto,
                                  BindingResult errors,
                                  Model model) {
        if (errors.hasErrors()) {
            return "warehouse-update";
        }

        try {
            Warehouse warehouse = warehouseMapper.fromWarehouseDtoToWarehouseObject(warehouseDto, id);

            warehouseService.update(warehouse, warehouse.getAddress());

            return "redirect:/warehouses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteWarehouse(@PathVariable int id, Model model) {
        try {
            warehouseService.delete(id);

            return "redirect:/warehouses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
}
