package com.team007.deliver_it.controllers.mvc;

import com.team007.deliver_it.controllers.AuthenticationHelper;
import com.team007.deliver_it.exceptions.AuthenticationFailureException;
import com.team007.deliver_it.exceptions.EntityNotFoundException;
import com.team007.deliver_it.exceptions.UnauthorizedOperationException;
import com.team007.deliver_it.mappers.ShipmentMapper;
import com.team007.deliver_it.models.*;
import com.team007.deliver_it.models.dtos.FilterDto;
import com.team007.deliver_it.models.dtos.ShipmentDto;
import com.team007.deliver_it.services.ShipmentService;
import com.team007.deliver_it.services.StatusService;
import com.team007.deliver_it.services.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static com.team007.deliver_it.models.User.checkIfEmployee;

@Controller
@RequestMapping("/shipments")
public class ShipmentMvcController {

    private final ShipmentService shipmentService;
    private final AuthenticationHelper authenticationHelper;
    private final StatusService statusService;
    private final WarehouseService warehouseService;
    private final ShipmentMapper shipmentMapper;

    @Autowired
    public ShipmentMvcController(ShipmentService shipmentService, AuthenticationHelper authenticationHelper, StatusService statusService, WarehouseService warehouseService, ShipmentMapper shipmentMapper) {
        this.shipmentService = shipmentService;
        this.authenticationHelper = authenticationHelper;
        this.statusService = statusService;
        this.warehouseService = warehouseService;
        this.shipmentMapper = shipmentMapper;
    }

    @ModelAttribute("statuses")
    public List<Status> populateStatuses() {
        return statusService.getAll();
    }

    @ModelAttribute("warehouses")
    public List<Warehouse> populateWarehouses() {
        return warehouseService.getAll();
    }

    @GetMapping
    public String showAllShipments(Model model) {
        try {
            model.addAttribute("shipments", shipmentService.getAll());
            model.addAttribute("filterDto", new FilterDto());

            return "shipments";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}")
    public String showSingleShipment(@PathVariable int id, Model model) {
        try {
            Shipment shipment = shipmentService.getById(id);
            boolean isCompleted = shipment.getStatus().getStatusName().equals("Completed");
            model.addAttribute("shipment", shipment);
            model.addAttribute("isCompleted", isCompleted);

            return "shipment";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "shipments";
        }
    }

    @GetMapping("/search")
    public String showSingleShipment(FilterDto filterDto, Model model) {
        if (filterDto.getId() == null) {
            return showAllShipments(model);
        }
        return showSingleShipment(filterDto.getId(), model);
    }

    @GetMapping("/show")
    public String showNextShipment(FilterDto filterDto, Model model) {
        try {
            if (filterDto.getWarehouseId() == 0) {
                return showAllShipments(model);
            }
            Shipment shipment = shipmentService.getNextShipmentByDestinationWarehouseId(filterDto.getWarehouseId());
            model.addAttribute("shipment", shipment);

            return "shipment";
        } catch (EntityNotFoundException e) {
            model.addAttribute("filterError", e.getMessage());
            return "shipments";
        }
    }

    @GetMapping("/filter")
    public String filter(@ModelAttribute FilterDto filterDto, Model model) {
        var filtered = shipmentService.filter(
                Optional.ofNullable(filterDto.getStatusId()),
                Optional.ofNullable(filterDto.getWarehouseId()));

        model.addAttribute("shipments", filtered);

        return "shipments";
    }

    @GetMapping("/new")
    public String showNewShipmentPage(Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        model.addAttribute("shipment", new ShipmentDto());
        try {
            checkIfEmployee(user);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
        return "shipment-new";
    }

    @PostMapping("/new")
    public String createShipment(@Valid @ModelAttribute("shipment") ShipmentDto shipmentDto,
                                 BindingResult errors,
                                 Model model) {
        if (errors.hasErrors()) {
            return "shipment-new";
        }

        try {
            Shipment shipment = shipmentMapper.fromShipmentDtoToShipmentObject(shipmentDto);
            shipmentService.create(shipment);

            return "redirect:/shipments";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }  catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "shipment-new";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditShipmentPage(@PathVariable int id, Model model) {
        try {
            Shipment shipment = shipmentService.getById(id);
            ShipmentDto shipmentDto = shipmentMapper.toDto(shipment);
            model.addAttribute("shipment", shipmentDto);

            return "shipment-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateShipment(@PathVariable int id,
                                 @Valid @ModelAttribute("shipment") ShipmentDto shipmentDto,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "shipment-update";
        }

        try {
            Shipment shipment = shipmentMapper.fromShipmentDtoToShipmentObject(shipmentDto, id);
            shipmentService.update(shipment);

            return "redirect:/shipments";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "shipment-update";
        }
    }
}
