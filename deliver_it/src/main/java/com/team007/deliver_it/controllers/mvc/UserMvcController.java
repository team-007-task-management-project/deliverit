package com.team007.deliver_it.controllers.mvc;

import com.team007.deliver_it.controllers.AuthenticationHelper;
import com.team007.deliver_it.exceptions.AuthenticationFailureException;
import com.team007.deliver_it.exceptions.DuplicateEntityException;
import com.team007.deliver_it.exceptions.EntityNotFoundException;
import com.team007.deliver_it.exceptions.UnauthorizedOperationException;
import com.team007.deliver_it.mappers.UserMapper;
import com.team007.deliver_it.models.*;
import com.team007.deliver_it.models.dtos.FilterDto;
import com.team007.deliver_it.models.dtos.RegisterDto;
import com.team007.deliver_it.models.dtos.UserDto;
import com.team007.deliver_it.models.dtos.WarehouseDto;
import com.team007.deliver_it.services.CityService;
import com.team007.deliver_it.services.ParcelService;
import com.team007.deliver_it.services.UserAddressService;
import com.team007.deliver_it.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UserMvcController {
    private final UserService userService;
    private final UserMapper userMapper;
    private final CityService cityService;
    private final ParcelService parcelService;
    private final AuthenticationHelper authenticationHelper;
    private final UserAddressService userAddressService;

    @Autowired
    public UserMvcController(UserService userService, UserMapper userMapper, CityService cityService, ParcelService parcelService, AuthenticationHelper authenticationHelper, UserAddressService userAddressService) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.cityService = cityService;
        this.parcelService = parcelService;
        this.authenticationHelper = authenticationHelper;
        this.userAddressService = userAddressService;
    }

    @ModelAttribute("customers")
    public int customersCount() {
        return userService.getAllCustomers().size();
    }

    @ModelAttribute("isCustomer")
    public boolean isCustomer(HttpSession session) {
        return (session.getAttribute("currentUser") != null && authenticationHelper.tryGetUser(session).getRole().getName().equals("Customer"));
    }

    @ModelAttribute("isEmployee")
    public boolean isEmployee(HttpSession session) {
        return (session.getAttribute("currentUser") != null && authenticationHelper.tryGetUser(session).getRole().getName().equals("Employee"));
    }

    @GetMapping
    public String showAllUsers(Model model) {
        try {
            model.addAttribute("users", userService.getAll());
            return "users";
        } catch (AuthenticationFailureException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/customers")
    public int showAllCustomers(Model model) {
        model.addAttribute("customers", userService.getAllCustomers());
        return userService.getAllCustomers().size();
    }

    @GetMapping("/profile")
    public String showProfile(Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        UserAddress userAddress = userAddressService.getByUserId(user.getId());
        List<Parcel> allParcels = parcelService.getAllForUser(user);
        List<Parcel> limitedParcels;
        if (allParcels.size() <= 5) {
            limitedParcels = allParcels;
        } else {
            limitedParcels = parcelService.getAllForUser(user).subList(0, 6);
        }
        model.addAttribute("parcels", parcelService.getAllForUser(user));
        model.addAttribute("limitedParcels", limitedParcels);
        model.addAttribute("user", user);
        model.addAttribute("userAddress", userAddress);

        return "profile";
    }

    @ModelAttribute("cities")
    public List<City> populateCities() {
        return cityService.getAll();
    }

    @ModelAttribute("isCustomer")
    public boolean populateIsCustomer(HttpSession session) {
        return (session.getAttribute("currentUser") != null && authenticationHelper.tryGetUser(session).getRole().getName().equals("Customer"));
    }

    @GetMapping("/customers/{id}/incoming-parcels")
    public String showIncomingParcelsPage(@PathVariable int id, Model model) {
        try {
            User user = userService.getById(id);
            List<Parcel> parcels = userService.getCustomerIncomingParcels(user, id);
            model.addAttribute("user", user);
            model.addAttribute("parcels", parcels);
            return "customer-incoming-parcels";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}")
    public String showSingleUser(@PathVariable int id, Model model) {
        try {
            User user = userService.getById(id);
            UserAddress userAddress = userAddressService.getByUserId(user.getId());
            model.addAttribute("user", user);
            model.addAttribute("userAddress", userAddress);
            model.addAttribute("parcels", parcelService.getAllForUser(user));
            model.addAttribute("filterDto", new FilterDto());

            return "user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/search")
    public String searchUser(HttpServletRequest servletRequest, Model model) {
        String keyWord = servletRequest.getParameter("keyWord");
        var result = userService.search(Optional.ofNullable(keyWord));
        model.addAttribute("users", result);

        return "users";
    }

    @GetMapping("/new")
    public String showCreateEmployeePage(Model model) {
        model.addAttribute("user", new RegisterDto());
        return "user-new";
    }

    @PostMapping("/new")
    public String createEmployee(@Valid @ModelAttribute("user") RegisterDto registerDto, BindingResult errors, Model model) {
        if (errors.hasErrors()) {
            return "user-new";
        }

        if (!registerDto.getPassword().equals(registerDto.getPasswordConfirm())) {
            errors.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
            return "users";
        }
        try {
            UserDto userDto = userMapper.fromRegisterDtoToUserDto(registerDto);
            User user = userMapper.fromUserDtoToCustomerObject(userDto);
            userService.createEmployee(user, userDto);

            return "redirect:/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("email", "duplicate_email", e.getMessage());
            return "user-new";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User user = userService.getById(id);
            UserAddress userAddress = userAddressService.getByUserId(user.getId());
            UserDto userDto = userMapper.fromUserToUserDto(user, userAddress);
            model.addAttribute("userId", id);
            model.addAttribute("user", userDto);
            return "user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("user") UserDto userDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User authUser;
        try {
            authUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "user-update";
        }

        try {
            User user = userMapper.fromUserDtoToUserObject(userDto, id);
            userService.update(user, authUser, userDto);
            if (authUser.getRole().getName().equals("Customer")) {
                return "redirect:/users/profile";
            }
            return String.format("redirect:/users/%s", id);
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_user", e.getMessage());
            return "user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }
}
