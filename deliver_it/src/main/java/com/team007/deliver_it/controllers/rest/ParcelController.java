package com.team007.deliver_it.controllers.rest;

import com.team007.deliver_it.controllers.AuthenticationHelper;
import com.team007.deliver_it.exceptions.EntityNotFoundException;
import com.team007.deliver_it.exceptions.UnauthorizedOperationException;
import com.team007.deliver_it.mappers.ParcelMapper;
import com.team007.deliver_it.models.*;
import com.team007.deliver_it.models.dtos.ParcelDto;
import com.team007.deliver_it.services.ParcelService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Optional;

import static com.team007.deliver_it.models.User.checkIfCustomer;
import static com.team007.deliver_it.models.User.checkIfEmployee;

@RestController
@RequestMapping("/api/parcels")
public class ParcelController {
    private final ParcelService service;
    private final AuthenticationHelper authenticationHelper;
    private final ParcelMapper parcelMapper;

    @Autowired
    public ParcelController(ParcelService service, AuthenticationHelper authenticationHelper, ParcelMapper parcelMapper) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.parcelMapper = parcelMapper;
    }

    @ApiOperation(value = "Returns all parcels.")
    @GetMapping
    public List<Parcel> getAll(@RequestHeader HttpHeaders headers) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            return service.getAll();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns a parcel by a given id.")
    @GetMapping("/{id}")
    public Parcel getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns all parcels filtered by a given status.")
    @GetMapping("/status")
    public List<Parcel> getByStatus(@RequestHeader HttpHeaders headers, @RequestParam int statusId) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfCustomer(authenticatedUser);

            int userId = authenticatedUser.getId();

            return service.getByStatus(statusId, userId);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns the status of a given parcel.")
    @GetMapping("/{parcelId}/status")
    public Status getStatus(@RequestHeader HttpHeaders headers, @PathVariable int parcelId) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);

            return service.getStatus(parcelId, authenticatedUser);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns all parcels filtered by userId, warehouseId, categoryId, minWeight and/or maxWeight.")
    @GetMapping("/filter")
    public List<Parcel> filter(@RequestHeader HttpHeaders headers,
                               @RequestParam(required = false) Optional<Integer> userId,
                               @RequestParam(required = false) Optional<Integer> warehouseId,
                               @RequestParam(required = false) Optional<Integer> categoryId,
                               @RequestParam(required = false) Optional<Double> minWeight,
                               @RequestParam(required = false) Optional<Double> maxWeight,
                               @RequestParam(required = false) Optional<Integer> statusId) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            return service.filter(userId, warehouseId, categoryId, minWeight, maxWeight, statusId);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/sort")
    @ApiOperation(value = "Sorts all parcels based on weight and/or arrivalDate.")
    public List<Parcel> sort(@RequestHeader HttpHeaders headers,
                             @RequestParam(required = false) Optional<String> weight,
                             @RequestParam(required = false) Optional<String> arrivalDate) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            return service.sort(weight, arrivalDate);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Creates a parcel.")
    @PostMapping
    public Parcel create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ParcelDto parcelDto) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            Parcel parcel = parcelMapper.fromParcelDtoToParcelObject(parcelDto);
            service.create(parcel);
            return parcel;

        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Updating the delivery type of an existing parcel.")
    @PutMapping("/{id}/customer")
    public Parcel customerUpdate(@RequestHeader HttpHeaders headers, @PathVariable int id, @Positive @RequestBody int deliveryType) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfCustomer(authenticatedUser);

            Parcel parcel = parcelMapper.updateDeliveryTypeInParcel(deliveryType, id);

            service.customerUpdate(authenticatedUser, parcel);
            return parcel;
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Updating an existing parcel.")
    @PutMapping("/{id}/employee")
    public Parcel employeeUpdate(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody ParcelDto parcelDto) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            Parcel parcel = parcelMapper.fromParcelDtoToParcelObject(parcelDto, id);

            service.employeeUpdate(parcel);
            return parcel;
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Deletes an existing parcel.")
    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            checkIfEmployee(authenticatedUser);

            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
