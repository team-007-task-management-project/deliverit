package com.team007.deliver_it.mappers;

import com.team007.deliver_it.models.Shipment;
import com.team007.deliver_it.models.Status;
import com.team007.deliver_it.models.dtos.ShipmentDto;
import com.team007.deliver_it.services.ShipmentService;
import com.team007.deliver_it.services.StatusService;
import com.team007.deliver_it.services.WarehouseService;
import org.springframework.stereotype.Component;

@Component
public class ShipmentMapper {

    private final ShipmentService shipmentService;
    private final WarehouseService warehouseService;
    private final StatusService statusService;

    public ShipmentMapper(ShipmentService shipmentService, WarehouseService warehouseService, StatusService statusService) {
        this.shipmentService = shipmentService;
        this.warehouseService = warehouseService;
        this.statusService = statusService;
    }

    public Shipment fromShipmentDtoToShipmentObject(ShipmentDto shipmentDto) {
        Shipment shipment = new Shipment();
        dtoToObject(shipmentDto, shipment);
        return shipment;
    }

    public Shipment fromShipmentDtoToShipmentObject(ShipmentDto shipmentDto, int id) {
        Shipment shipment = shipmentService.getById(id);
        dtoToObject(shipmentDto, shipment);
        return shipment;
    }

    private void dtoToObject(ShipmentDto shipmentDto, Shipment shipment) {
        Status status = statusService.getById(shipmentDto.getStatusId());
        shipment.setDepartureDate(shipmentDto.getDepartureDate());
        shipment.setArrivalDate(shipmentDto.getArrivalDate());
        shipment.setOriginWarehouse(warehouseService.getById(shipmentDto.getOriginWarehouseId()));
        shipment.setDestinationWarehouse(warehouseService.getById(shipmentDto.getDestinationWarehouseId()));
        shipment.setStatus(status);
    }

    public ShipmentDto toDto(Shipment shipment) {
        ShipmentDto shipmentDto = new ShipmentDto();
        shipmentDto.setDepartureDate(shipment.getDepartureDate());
        shipmentDto.setArrivalDate(shipment.getArrivalDate());
        shipmentDto.setOriginWarehouseId(shipment.getOriginWarehouse().getId());
        shipmentDto.setDestinationWarehouseId(shipment.getDestinationWarehouse().getId());
        shipmentDto.setStatusId(shipment.getStatus().getId());
        return shipmentDto;
    }
}
