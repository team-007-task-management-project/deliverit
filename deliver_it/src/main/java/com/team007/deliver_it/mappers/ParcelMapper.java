package com.team007.deliver_it.mappers;

import com.team007.deliver_it.models.*;
import com.team007.deliver_it.models.dtos.ParcelDto;
import com.team007.deliver_it.services.*;
import org.springframework.stereotype.Component;

@Component
public class ParcelMapper {

    private final ParcelService parcelService;
    private final UserService userService;
    private final WarehouseService warehouseService;
    private final ShipmentService shipmentService;
    private final CategoryService categoryService;

    private final DeliveryTypeService deliveryTypeService;

    public ParcelMapper(ParcelService parcelService, UserService userService, WarehouseService warehouseService, ShipmentService shipmentService, CategoryService categoryService, DeliveryTypeService deliveryTypeService) {
        this.parcelService = parcelService;
        this.userService = userService;
        this.warehouseService = warehouseService;
        this.shipmentService = shipmentService;
        this.categoryService = categoryService;
        this.deliveryTypeService = deliveryTypeService;
    }

    public Parcel fromParcelDtoToParcelObject(ParcelDto parcelDto) {
        Parcel parcel = new Parcel();
        dtoToObject(parcelDto, parcel);
        return parcel;
    }

    public Parcel fromParcelDtoToParcelObject(ParcelDto parcelDto, int id) {
        Parcel parcel = parcelService.getById(id);
        dtoToObject(parcelDto, parcel);
        return parcel;
    }

    public Parcel updateDeliveryTypeInParcel(int deliveryType, int id) {
        Parcel parcel = parcelService.getById(id);
        DeliveryType existingDeliveryType = deliveryTypeService.getById(deliveryType);
        parcel.setDeliveryType(existingDeliveryType);
        return parcel;
    }

    public ParcelDto toDto(Parcel parcel) {
        ParcelDto parcelDto = new ParcelDto();
        parcelDto.setUserId(parcel.getUser().getId());
        parcelDto.setWarehouseId(parcel.getWarehouse().getId());
        parcelDto.setWeight(parcel.getWeight());
        parcelDto.setCategoryId(parcel.getCategory().getId());
        parcelDto.setShipmentId(parcel.getShipment().getId());
        parcelDto.setDeliveryTypeId(parcel.getDeliveryType().getId());
        return parcelDto;
    }

    private void dtoToObject(ParcelDto parcelDto, Parcel parcel) {
        User user = userService.getById(parcelDto.getUserId());
        Warehouse warehouse = warehouseService.getById(parcelDto.getWarehouseId());
        Shipment shipment = shipmentService.getById(parcelDto.getShipmentId());
        Category category = categoryService.getById(parcelDto.getCategoryId());
        DeliveryType deliveryType = deliveryTypeService.getById(parcelDto.getDeliveryTypeId());
        parcel.setUser(user);
        parcel.setWarehouse(warehouse);
        parcel.setWeight(parcelDto.getWeight());
        parcel.setShipment(shipment);
        parcel.setDeliveryType(deliveryType);
        parcel.setCategory(category);
    }
}
