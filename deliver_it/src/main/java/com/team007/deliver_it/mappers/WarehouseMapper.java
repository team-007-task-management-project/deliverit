package com.team007.deliver_it.mappers;

import com.team007.deliver_it.models.Address;
import com.team007.deliver_it.models.Warehouse;
import com.team007.deliver_it.models.dtos.WarehouseDto;
import com.team007.deliver_it.services.WarehouseService;
import org.springframework.stereotype.Component;

@Component
public class WarehouseMapper {

    private final WarehouseService warehouseService;
    private final AddressMapper addressMapper;

    public WarehouseMapper(WarehouseService warehouseService, AddressMapper addressMapper) {
        this.warehouseService = warehouseService;
        this.addressMapper = addressMapper;
    }

    public Warehouse fromWarehouseDtoToWarehouseObject(Address address) {
        Warehouse warehouse = new Warehouse();
        warehouse.setAddress(address);
        return warehouse;
    }

    public Warehouse fromWarehouseDtoToWarehouseObject(WarehouseDto warehouseDto, int id) {
        Warehouse warehouse = warehouseService.getById(id);
        dtoToObject(warehouseDto, warehouse);
        return warehouse;
    }

    public WarehouseDto toDto(Warehouse warehouse) {
        WarehouseDto warehouseDto = new WarehouseDto();
        warehouseDto.setStreetName(warehouse.getAddress().getStreetName());
        warehouseDto.setCityId(warehouse.getAddress().getCity().getId());
        return warehouseDto;
    }

    public void dtoToObject(WarehouseDto warehouseDto, Warehouse warehouse) {
        Address address = warehouse.getAddress();
        addressMapper.dtoToObject(warehouseDto, address);
        warehouse.setAddress(address);
    }
}
