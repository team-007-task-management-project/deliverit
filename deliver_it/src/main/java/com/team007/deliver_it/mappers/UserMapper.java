package com.team007.deliver_it.mappers;

import com.team007.deliver_it.models.Role;
import com.team007.deliver_it.models.User;
import com.team007.deliver_it.models.UserAddress;
import com.team007.deliver_it.models.dtos.RegisterDto;
import com.team007.deliver_it.models.dtos.UserDto;
import com.team007.deliver_it.services.RoleService;
import com.team007.deliver_it.services.UserService;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private final UserService userService;
    private final RoleService roleService;

    public UserMapper(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    public User fromUserDtoToCustomerObject(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user, 1);
        return user;
    }

    public User fromUserDtoToEmployeeObject(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user, 2);
        return user;
    }

    public User fromUserDtoToUserObject(UserDto userDto, int id) {
        User user = userService.getById(id);
        dtoToObject(userDto, user, user.getRole().getId());
        return user;
    }

    public UserDto fromRegisterDtoToUserDto(RegisterDto registerDto) {
        UserDto userDto = new UserDto();
        userDto.setEmail(registerDto.getEmail());
        userDto.setPassword(registerDto.getPassword());
        userDto.setFirstName(registerDto.getFirstName());
        userDto.setLastName(registerDto.getLastName());
        userDto.setStreetName(registerDto.getStreetName());
        userDto.setCityId(registerDto.getCityId());

        return userDto;
    }

    public UserDto fromUserToUserDto(User user, UserAddress userAddress) {
        UserDto userDto = new UserDto();
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());
        if (userAddress == null) {
            userDto.setStreetName("");
            userDto.setCityId(9999);
        } else {
            userDto.setStreetName(userAddress.getAddress().getStreetName());
            userDto.setCityId(userAddress.getAddress().getCity().getId());
        }

        return userDto;
    }

    private void dtoToObject(UserDto userDto, User user, int roleId) {
        Role role = roleService.getById(roleId);
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setRole(role);
    }
}
