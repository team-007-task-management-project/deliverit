package com.team007.deliver_it.mappers;

import com.team007.deliver_it.models.Address;
import com.team007.deliver_it.models.City;
import com.team007.deliver_it.models.UserAddress;
import com.team007.deliver_it.models.dtos.UserDto;
import com.team007.deliver_it.models.dtos.WarehouseDto;
import com.team007.deliver_it.services.CityService;
import org.springframework.stereotype.Component;

@Component
public class AddressMapper {

    private final CityService cityService;

    public AddressMapper(CityService cityService) {
        this.cityService = cityService;
    }

    public Address fromUserDtoToAddressObject(UserDto userDto) {
        Address address = new Address();
        dtoToObject(userDto, address);
        return address;
    }

    public Address fromWarehouseDtoToAddressObject(WarehouseDto warehouseDto) {
        Address address = new Address();
        dtoToObject(warehouseDto, address);
        return address;
    }

    public Address updateAddress(UserAddress userAddress, UserDto userDto) {
        Address address = userAddress.getAddress();
        dtoToObject(userDto, address);
        return address;
    }

    public void dtoToObject(WarehouseDto warehouseDto, Address address) {
        City city = cityService.getById(warehouseDto.getCityId());
        String streetName = warehouseDto.getStreetName();
        address.setCity(city);
        address.setStreetName(streetName);
    }

    private void dtoToObject(UserDto userDto, Address address) {
        City city = cityService.getById(userDto.getCityId());
        String streetName = userDto.getStreetName();
        address.setCity(city);
        address.setStreetName(streetName);
    }
}
