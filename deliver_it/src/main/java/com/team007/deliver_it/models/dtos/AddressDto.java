package com.team007.deliver_it.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class AddressDto {
    public static final String STREET_NAME_INVALID = "Street name cannot be empty";
    public static final String STREET_NAME_LENGTH_INVALID = "Street name should be between 2 and 50 symbols";
    public static final String CITY_ID_ERROR_MESSAGE = "City id should be positive.";
    @NotNull(message = STREET_NAME_INVALID)
    @Size(min = 2, max = 50, message = STREET_NAME_LENGTH_INVALID)
    private String streetName;

    @Positive(message = CITY_ID_ERROR_MESSAGE)
    private int cityId;

    public AddressDto() {
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
}
