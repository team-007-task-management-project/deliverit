package com.team007.deliver_it.models.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.team007.deliver_it.models.dtos.UserDto.*;

public class RegisterDto extends LoginDto {

    public static final String PASSWORD_CONFIRMATION_INVALID = "Password confirmation must match Password";
    @NotEmpty(message = PASSWORD_CONFIRMATION_INVALID)
    private String passwordConfirm;

    @NotNull(message = FIRST_NAME_INVALID)
    @Size(min = 2, max = 30, message = FIRST_NAME_LENGTH_INVALID)
    private String firstName;

    @NotNull(message = LAST_NAME_INVALID)
    @Size(min = 2, max = 30, message = LAST_NAME_LENGTH_INVALID)
    private String lastName;

    private String streetName;

    private int cityId;

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
}
