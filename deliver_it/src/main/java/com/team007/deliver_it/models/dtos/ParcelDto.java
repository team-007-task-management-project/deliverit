package com.team007.deliver_it.models.dtos;

import javax.validation.constraints.Positive;

public class ParcelDto {
    private static final String USER_ID_INVALID = "User ID should be positive.";
    private static final String WAREHOUSE_ID_INVALID = "Warehouse ID should be positive.";
    private static final String WEIGHT_INVALID = "Weight should be positive.";
    private static final String CATEGORY_ID_INVALID = "Category ID should be positive.";
    private static final String SHIPMENT_ID_INVALID = "Shipment ID should be positive.";
    private static final String DELIVERY_TYPE_ID_INVALID = "Delivery Type ID should be positive.";
    @Positive(message = USER_ID_INVALID)
    private int userId;

    @Positive(message = WAREHOUSE_ID_INVALID)
    private int warehouseId;

    @Positive(message = WEIGHT_INVALID)
    private double weight;

    @Positive(message = CATEGORY_ID_INVALID)
    private int categoryId;

    @Positive(message = SHIPMENT_ID_INVALID)
    private int shipmentId;

    @Positive(message = DELIVERY_TYPE_ID_INVALID)
    private int deliveryTypeId;

    public ParcelDto() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(int shipmentId) {
        this.shipmentId = shipmentId;
    }

    public int getDeliveryTypeId() {
        return deliveryTypeId;
    }

    public void setDeliveryTypeId(int deliveryTypeId) {
        this.deliveryTypeId = deliveryTypeId;
    }
}
