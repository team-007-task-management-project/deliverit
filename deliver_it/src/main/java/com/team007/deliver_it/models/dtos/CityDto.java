package com.team007.deliver_it.models.dtos;

import com.team007.deliver_it.models.City;

public class CityDto {

    private String name;

    private int countryId;

    public CityDto(City city) {
        setName(city.getName());
        setCountryId(city.getCountry().getId());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }
}