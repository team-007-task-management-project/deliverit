package com.team007.deliver_it.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static com.team007.deliver_it.models.dtos.AddressDto.*;

public class WarehouseDto {
    @NotNull(message = STREET_NAME_INVALID)
    @Size(min = 2, max = 50, message = STREET_NAME_LENGTH_INVALID)
    private String streetName;

    @Positive(message = CITY_ID_ERROR_MESSAGE)
    private int cityId;

    public WarehouseDto() {
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
}
