package com.team007.deliver_it.models;

import com.team007.deliver_it.exceptions.UnauthorizedOperationException;

import javax.persistence.*;
import javax.validation.constraints.Email;

@Entity
@Table(name = "users")
public class User {

    private static final String NOT_AN_EMPLOYEE_ERROR_MESSAGE = "You must be an employee in order to execute this operation.";
    private static final String NOT_A_CUSTOMER_ERROR_MESSAGE = "You must be a customer in order to execute this operation.";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "password")
    private String password;

    @Email
    @Column(name = "email")
    private String email;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static void checkIfEmployee(User authenticatedUser) {
        if (!authenticatedUser.getRole().getName().equals("Employee")) {
            throw new UnauthorizedOperationException(NOT_AN_EMPLOYEE_ERROR_MESSAGE);
        }
    }

    public static void checkIfCustomer(User authenticatedUser) {
        if (!authenticatedUser.getRole().getName().equals("Customer")) {
            throw new UnauthorizedOperationException(NOT_A_CUSTOMER_ERROR_MESSAGE);
        }
    }
}
