package com.team007.deliver_it.models;

import javax.persistence.*;

@Entity
@Table(name = "delivery_types")
public class DeliveryType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "delivery_type_id")
    private int id;

    @Column(name = "delivery_type_name")
    private String deliveryTypeName;

    public DeliveryType() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeliveryTypeName() {
        return deliveryTypeName;
    }

    public void setDeliveryTypeName(String deliveryType) {
        this.deliveryTypeName = deliveryType;
    }
}
