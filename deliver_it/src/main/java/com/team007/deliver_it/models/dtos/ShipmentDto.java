package com.team007.deliver_it.models.dtos;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

public class ShipmentDto {

    private static final String DEPARTURE_DATE_ERROR_MESSAGE = "Departure date should be in the present or future.";
    private static final String ARRIVAL_DATE_ERROR_MESSAGE = "Arrival date should be in the future.";
    private static final String ORIGIN_WAREHOUSE_ERROR_MESSAGE = "Origin warehouse id should be positive.";
    private static final String DESTINATION_WAREHOUSE_ERROR_MESSAGE = "Destination warehouse id should be positive.";
    private static final String STATUS_ID_ERROR_MESSAGE = "Status id should be positive.";

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @FutureOrPresent(message = DEPARTURE_DATE_ERROR_MESSAGE)
    @NotNull
    private LocalDate departureDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Future(message = ARRIVAL_DATE_ERROR_MESSAGE)
    @NotNull
    private LocalDate arrivalDate;

    @Positive(message = ORIGIN_WAREHOUSE_ERROR_MESSAGE)
    private int originWarehouseId;

    @Positive(message = DESTINATION_WAREHOUSE_ERROR_MESSAGE)
    private int destinationWarehouseId;

    @Positive(message = STATUS_ID_ERROR_MESSAGE)
    private int statusId;

    public ShipmentDto() {
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDate getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(LocalDate arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public int getOriginWarehouseId() {
        return originWarehouseId;
    }

    public void setOriginWarehouseId(int originWarehouseId) {
        this.originWarehouseId = originWarehouseId;
    }

    public int getDestinationWarehouseId() {
        return destinationWarehouseId;
    }

    public void setDestinationWarehouseId(int destinationWarehouseId) {
        this.destinationWarehouseId = destinationWarehouseId;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }
}
