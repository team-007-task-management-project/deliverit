package com.team007.deliver_it.models.dtos;

import javax.validation.constraints.*;

import static com.team007.deliver_it.models.dtos.AddressDto.*;

public class UserDto {

    public static final String FIRST_NAME_INVALID = "First name cannot be empty";
    public static final String LAST_NAME_INVALID = "Last name cannot be empty";
    public static final String FIRST_NAME_LENGTH_INVALID = "First name should be between 2 and 30 symbols";
    public static final String LAST_NAME_LENGTH_INVALID = "Last name should be between 2 and 30 symbols";
    @NotNull(message = FIRST_NAME_INVALID)
    @Size(min = 2, max = 30, message = FIRST_NAME_LENGTH_INVALID)
    private String firstName;

    @NotNull(message = LAST_NAME_INVALID)
    @Size(min = 2, max = 30, message = LAST_NAME_LENGTH_INVALID)
    private String lastName;

    @Email
    private String email;

    private String password;

    private String streetName;

    private int cityId;

    public UserDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
