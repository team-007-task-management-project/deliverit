package com.team007.deliver_it.repositories;

import com.team007.deliver_it.models.Country;

import java.util.List;

public interface CountryRepository {

    List<Country> getAll();

    Country getById(int id);
}
