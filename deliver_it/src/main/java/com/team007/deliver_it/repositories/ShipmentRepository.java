package com.team007.deliver_it.repositories;

import com.team007.deliver_it.models.Shipment;
import com.team007.deliver_it.models.Warehouse;

import java.util.List;
import java.util.Optional;

public interface ShipmentRepository {

    List<Shipment> getAll();

    Shipment getById(int id);

    void create(Shipment shipment);

    void update(Shipment shipment);

    boolean hasParcels(Shipment shipment);

    void delete(int id);

    List<Shipment> filter(Optional<Integer> statusId, Optional<Integer> warehouseId, Optional<Integer> userId);

    List<Shipment> filter(Optional<Integer> statusId, Optional<Integer> warehouseId);

    List<Shipment> getByStatus(int statusId);

    Shipment getNextShipmentByDestinationWarehouseId(int destinationWarehouseId, Warehouse destinationWarehouse);
}
