package com.team007.deliver_it.repositories;

import com.team007.deliver_it.exceptions.EntityNotFoundException;
import com.team007.deliver_it.models.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list();
        }
    }

    @Override
    public List<User> getAllCustomers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where role.id = 1", User.class);
            return query.list();
        }
    }

    @Override
    public List<User> getAllEmployees() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where role.id = 2", User.class);
            return query.list();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "email", email);
            }

            return result.get(0);
        }
    }

    @Override
    public void createCustomer(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
        }
    }

    @Override
    public void createEmployee(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        User userToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<User> filter(Optional<String> firstName, Optional<String> lastName, Optional<String> email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where (:firstName = '%%' OR firstName like :firstName) AND (:lastName = '%%' OR lastName like :lastName) AND (:email = '%%' OR email like :email)", User.class);
            query.setParameter("firstName", "%" + firstName.orElse("") + "%");
            query.setParameter("lastName", "%" + lastName.orElse("") + "%");
            query.setParameter("email", "%" + email.orElse("") + "%");

            return query.list();
        }
    }

    @Override
    public List<Parcel> getCustomerIncomingParcels(int customerId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("select p from Parcel p inner join Shipment s on s.id = p.shipment.id where p.user.id = :customerId and s.status.id = 2", Parcel.class);
            query.setParameter("customerId", customerId);

            return query.list();
        }
    }

    @Override
    public List<User> search(Optional<String> keyWord) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where ((:keyWord = '%%') OR (firstName like :keyWord) OR (lastName like :keyWord) OR (email like :keyWord))", User.class);
            query.setParameter("keyWord", "%" + keyWord.orElse("") + "%");

            return query.list();
        }
    }

}
