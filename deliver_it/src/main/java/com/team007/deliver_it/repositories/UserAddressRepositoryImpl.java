package com.team007.deliver_it.repositories;

import com.team007.deliver_it.models.UserAddress;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserAddressRepositoryImpl implements UserAddressRepository {

    private final SessionFactory sessionFactory;

    public UserAddressRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(UserAddress userAddress) {
        try (Session session = sessionFactory.openSession()) {
            session.save(userAddress);
        }
    }

    @Override
    public UserAddress getByUserID(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserAddress> query = session.createQuery("from UserAddress where user.id = :userId", UserAddress.class);
            query.setParameter("userId", userId);

            List<UserAddress> result = query.list();
            if (result.size() == 0) {
                return null;
            }

            return result.get(0);
        }
    }

    @Override
    public void delete(UserAddress userAddress) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userAddress);
            session.getTransaction().commit();
        }
    }
}
