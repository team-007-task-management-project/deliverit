package com.team007.deliver_it.repositories;

import com.team007.deliver_it.exceptions.EntityNotFoundException;
import com.team007.deliver_it.models.Parcel;
import com.team007.deliver_it.models.Shipment;
import com.team007.deliver_it.models.Warehouse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public class ShipmentRepositoryImpl implements ShipmentRepository {

    private final SessionFactory sessionFactory;

    public ShipmentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Shipment> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("from Shipment", Shipment.class);
            return query.list();
        }
    }

    @Override
    public Shipment getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Shipment shipment = session.get(Shipment.class, id);
            if (shipment == null) {
                throw new EntityNotFoundException("Shipment", id);
            }
            return shipment;
        }
    }

    @Override
    public List<Shipment> getByStatus(int statusId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("from Shipment where status.id = :statusId", Shipment.class);
            query.setParameter("statusId", statusId);
            return query.list();
        }
    }

    @Override
    public Shipment getNextShipmentByDestinationWarehouseId(int destinationWarehouseId, Warehouse destinationWarehouse) {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("from Shipment where destinationWarehouse.id = :destinationWarehouseId and status.id != 3 order by arrivalDate", Shipment.class);
            query.setParameter("destinationWarehouseId", destinationWarehouseId);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Shipment", "destination warehouse address", String.format("%s, %s", destinationWarehouse.getAddress().getStreetName(), destinationWarehouse.getAddress().getCity().getName()));
            }
            return query.list().get(0);
        }
    }

    @Override
    public void create(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            session.save(shipment);
        }
    }

    @Override
    public void update(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(shipment);
            session.getTransaction().commit();
        }
    }

    public boolean hasParcels(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("from Parcel where shipment.id = :shipmentId", Parcel.class);
            query.setParameter("shipmentId", shipment.getId());
            return query.list().size() != 0;
        }
    }

    @Override
    public void delete(int id) {
        Shipment shipmentToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(shipmentToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Shipment> filter(Optional<Integer> statusId, Optional<Integer> warehouseId, Optional<Integer> userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("select s from Shipment s inner join Parcel p on s.id = p.shipment.id where (:userId = 0 OR p.user.id = :userId) and (:warehouseId = 0 OR s.originWarehouse.id = :warehouseId) and (:statusId = 0 OR s.status.id = :statusId)", Shipment.class);
            query.setParameter("warehouseId", warehouseId.orElse(0));
            query.setParameter("userId", userId.orElse(0));
            query.setParameter("statusId", statusId.orElse(0));

            return query.list();
        }
    }

    @Override
    public List<Shipment> filter(Optional<Integer> statusId, Optional<Integer> warehouseId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("from Shipment where (:statusId = 0 OR status.id = :statusId) and (:warehouseId = 0 OR originWarehouse.id = :warehouseId)", Shipment.class);
            query.setParameter("warehouseId", warehouseId.orElse(0));
            query.setParameter("statusId", statusId.orElse(0));

            return query.list();
        }
    }
}
