package com.team007.deliver_it.repositories;

import com.team007.deliver_it.models.Parcel;
import com.team007.deliver_it.models.Status;
import com.team007.deliver_it.models.User;

import java.util.List;
import java.util.Optional;

public interface ParcelRepository {

    List<Parcel> getAll();

    List<Parcel> getAllForUser(User user);

    Parcel getById(int id);

    void create(Parcel parcel);

    void update(Parcel parcel);

    void delete(int id);

    List<Parcel> filter(Optional<Integer> userId, Optional<Integer> warehouseId, Optional<Integer> categoryId, Optional<Double> minWeight, Optional<Double> maxWeight, Optional<Integer> statusId);

    List<Parcel> getByStatus(int statusId, int userId);

    Status getStatus(int parcelId);

    List<Parcel> sort(Optional<String> weight, Optional<String> arrivalDate);
}
