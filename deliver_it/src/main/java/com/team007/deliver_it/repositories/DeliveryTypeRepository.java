package com.team007.deliver_it.repositories;

import com.team007.deliver_it.models.DeliveryType;

import java.util.List;

public interface DeliveryTypeRepository {

    List<DeliveryType> getAll();

    DeliveryType getById(int id);
}
