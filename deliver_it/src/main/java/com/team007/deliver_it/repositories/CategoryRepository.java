package com.team007.deliver_it.repositories;

import com.team007.deliver_it.models.Category;

import java.util.List;

public interface CategoryRepository {

    List<Category> getAll();

    Category getById(int id);
}
