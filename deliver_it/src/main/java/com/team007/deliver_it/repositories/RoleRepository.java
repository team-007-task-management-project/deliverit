package com.team007.deliver_it.repositories;

import com.team007.deliver_it.models.Role;

import java.util.List;

public interface RoleRepository {
    List<Role> getAll();

    Role getById(int id);
}
