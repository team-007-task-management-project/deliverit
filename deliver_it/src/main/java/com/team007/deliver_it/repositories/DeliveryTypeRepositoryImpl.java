package com.team007.deliver_it.repositories;

import com.team007.deliver_it.exceptions.EntityNotFoundException;
import com.team007.deliver_it.models.Country;
import com.team007.deliver_it.models.DeliveryType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DeliveryTypeRepositoryImpl implements DeliveryTypeRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public DeliveryTypeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<DeliveryType> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<DeliveryType> query = session.createQuery("from DeliveryType", DeliveryType.class);
            return query.list();
        }
    }

    @Override
    public DeliveryType getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            DeliveryType deliveryType = session.get(DeliveryType.class, id);
            if (deliveryType == null) {
                throw new EntityNotFoundException("Delivery type", id);
            }
            return deliveryType;
        }
    }
}
