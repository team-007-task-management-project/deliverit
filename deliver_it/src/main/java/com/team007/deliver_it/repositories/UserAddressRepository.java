package com.team007.deliver_it.repositories;

import com.team007.deliver_it.models.UserAddress;

public interface UserAddressRepository {

    void create(UserAddress userAddress);

    UserAddress getByUserID(int userId);

    void delete(UserAddress userAddress);
}
