package com.team007.deliver_it.repositories;

import com.team007.deliver_it.models.Address;
import com.team007.deliver_it.models.User;
import com.team007.deliver_it.models.UserAddress;

import java.util.List;

public interface AddressRepository {

    void create(Address address);

    Address getById(int id);

    void update(Address address);

    void delete(Address address);
}
