package com.team007.deliver_it.repositories;

import com.team007.deliver_it.models.Parcel;
import com.team007.deliver_it.models.User;
import java.util.List;
import java.util.Optional;

public interface UserRepository {

    List<User> getAll();

    List<User> getAllCustomers();

    List<User> getAllEmployees();

    User getById(int id);

    User getByEmail(String email);

    void createCustomer(User user);

    void createEmployee(User user);

    void update(User user);

    void delete(int id);

    List<User> filter(Optional<String> firstName, Optional<String> lastName, Optional<String> email);

    List<Parcel> getCustomerIncomingParcels(int customerId);

    List<User> search(Optional<String> keyWord);
}
