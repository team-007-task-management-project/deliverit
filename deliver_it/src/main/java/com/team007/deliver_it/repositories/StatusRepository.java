package com.team007.deliver_it.repositories;

import com.team007.deliver_it.models.Status;

import java.util.List;

public interface StatusRepository {
    List<Status> getAll();

    Status getById(int id);
}
