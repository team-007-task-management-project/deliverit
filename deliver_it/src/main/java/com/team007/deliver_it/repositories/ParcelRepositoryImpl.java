package com.team007.deliver_it.repositories;

import com.team007.deliver_it.exceptions.EntityNotFoundException;
import com.team007.deliver_it.models.*;
import com.team007.deliver_it.models.dtos.ParcelDto;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ParcelRepositoryImpl implements ParcelRepository {

    private final SessionFactory sessionFactory;

    public ParcelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Parcel> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("from Parcel", Parcel.class);
            return query.list();
        }
    }

    @Override
    public List<Parcel> getAllForUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("select p from Parcel p inner join Shipment s on p.shipment.id = s.id where p.user.id = :userId order by s.arrivalDate desc", Parcel.class);
            query.setParameter("userId", user.getId());
            return query.list();
        }
    }

    @Override
    public Parcel getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Parcel parcel = session.get(Parcel.class, id);
            if (parcel == null) {
                throw new EntityNotFoundException("Parcel", id);
            }
            return parcel;
        }
    }

    @Override
    public List<Parcel> getByStatus(int statusId, int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("from Parcel where user.id = :userId and shipment.status.id = :statusId", Parcel.class);
            query.setParameter("userId", userId);
            query.setParameter("statusId", statusId);
            return query.list();
        }
    }

    @Override
    public Status getStatus(int parcelId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Status> query = session.createQuery("select s.status from Shipment s inner join Parcel p on s.id = p.shipment.id where (p.id = :parcelId)", Status.class);
            query.setParameter("parcelId", parcelId);
            return query.list().get(0);
        }
    }

    @Override
    public void create(Parcel parcel) {
        try (Session session = sessionFactory.openSession()) {
            session.save(parcel);
        }
    }

    @Override
    public void update(Parcel parcel) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(parcel);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Parcel parcelToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(parcelToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Parcel> filter(Optional<Integer> userId, Optional<Integer> warehouseId, Optional<Integer> categoryId, Optional<Double> minWeight, Optional<Double> maxWeight, Optional<Integer> statusId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Parcel> query = session.createQuery("from Parcel where (:userId = 0 OR user.id = :userId) and (:warehouseId = 0 OR warehouse.id = :warehouseId) and (:categoryId = 0 OR category.id = :categoryId) and (:minWeight = 0.0 OR weight >= :minWeight) and (:maxWeight = 0.0 OR weight <= :maxWeight) and (:statusId = 0 OR shipment.status.id = :statusId)", Parcel.class);
            query.setParameter("userId", userId.orElse(0));
            query.setParameter("warehouseId", warehouseId.orElse(0));
            query.setParameter("categoryId", categoryId.orElse(0));
            query.setParameter("minWeight", minWeight.orElse(0.0));
            query.setParameter("maxWeight", maxWeight.orElse(0.0));
            query.setParameter("statusId", statusId.orElse(0));

            return query.list();
        }
    }

    @Override
    public List<Parcel> sort(Optional<String> weight, Optional<String> arrivalDate) {
        try (Session session = sessionFactory.openSession()) {

            String baseQuery = "select p from Parcel p";

            if (arrivalDate.isPresent() && weight.isPresent() && (arrivalDate.equals(Optional.of("asc")) || arrivalDate.equals(Optional.of("desc"))) && (weight.equals(Optional.of("asc")) || weight.equals(Optional.of("desc")))) {
                baseQuery += " inner join Shipment s on p.shipment.id = s.id order by weight " + weight.get() + ", s.arrivalDate " + arrivalDate.get();
            } else if (arrivalDate.isPresent()) {
                if (arrivalDate.equals(Optional.of("asc")) || arrivalDate.equals(Optional.of("desc"))) {
                    baseQuery += " inner join Shipment s on p.shipment.id = s.id order by s.arrivalDate " + arrivalDate.get();
                }
            } else if (weight.isPresent()) {
                if (weight.equals(Optional.of("asc")) || weight.equals(Optional.of("desc"))) {
                    baseQuery += " order by weight " + weight.get();
                }
            }

            Query<Parcel> query = session.createQuery(baseQuery, Parcel.class);

            return query.list();
        }
    }
}

