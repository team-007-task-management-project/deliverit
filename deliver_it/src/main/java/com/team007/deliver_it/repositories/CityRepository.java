package com.team007.deliver_it.repositories;

import com.team007.deliver_it.models.City;

import java.util.List;

public interface CityRepository {

    List<City> getAll();

    City getById(int id);
}
