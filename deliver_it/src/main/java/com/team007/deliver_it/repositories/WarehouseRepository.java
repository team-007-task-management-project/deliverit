package com.team007.deliver_it.repositories;

import com.team007.deliver_it.models.Warehouse;
import java.util.List;

public interface WarehouseRepository {

    List<Warehouse> getAll();

    Warehouse getById(int id);

    void create(Warehouse warehouse);

    void update(Warehouse warehouse);

    void delete(int id);
}
