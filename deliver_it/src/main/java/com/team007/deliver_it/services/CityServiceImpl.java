package com.team007.deliver_it.services;

import com.team007.deliver_it.models.City;
import com.team007.deliver_it.models.dtos.CityDto;
import com.team007.deliver_it.repositories.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CityServiceImpl implements CityService {

    private final CityRepository repository;

    @Autowired
    public CityServiceImpl(CityRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<City> getAll() {
        return repository.getAll();
    }

    @Override
    public City getById(int id) {
        return repository.getById(id);
    }
}
