package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Status;

import java.util.List;

public interface StatusService {
    List<Status> getAll();

    Status getById(int id);
}
