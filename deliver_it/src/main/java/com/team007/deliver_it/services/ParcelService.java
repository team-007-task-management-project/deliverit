package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Parcel;
import com.team007.deliver_it.models.Status;
import com.team007.deliver_it.models.User;

import java.util.List;
import java.util.Optional;

public interface ParcelService {

    List<Parcel> getAll();

    Parcel getById(int id);

    List<Parcel> getByStatus(int statusId, int userId);

    Status getStatus(int parcelId, User authenticatedUser);

    List<Parcel> filter(Optional<Integer> userId, Optional<Integer> warehouseId, Optional<Integer> categoryId, Optional<Double> minWeight, Optional<Double> maxWeight, Optional<Integer> statusId);

    List<Parcel> sort(Optional<String> weight, Optional<String> arrivalDate);

    void create(Parcel parcel);

    void customerUpdate(User authenticatedUser, Parcel parcel);

    void employeeUpdate(Parcel parcel);

    void delete(int id);

    List<Parcel> getAllForUser(User user);
}
