package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Address;

public interface AddressService {
    void create(Address address);

    Address getById(int id);

    void update(Address address);

    void delete(Address address);
}
