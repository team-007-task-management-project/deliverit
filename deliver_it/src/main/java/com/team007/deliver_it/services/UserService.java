package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Parcel;
import com.team007.deliver_it.models.User;
import com.team007.deliver_it.models.dtos.UserDto;
import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> getAll();

    List<User> getAllCustomers();

    List<User> getAllEmployees();

    User getById(int id);

    User getByEmail(String email);

    List<Parcel> getCustomerIncomingParcels(User authenticatedUser, int customerId);

    List<User> filter(Optional<String> firstName, Optional<String> lastName, Optional<String> email);

    List<User> search(Optional<String> keyWord);

    void createCustomer(User user, UserDto userDto);

    void createEmployee(User user, UserDto userDto);

    void update(User user, User authenticatedUser, UserDto userDto);

    void delete(int id, User authenticatedUser);

}
