package com.team007.deliver_it.services;

import com.team007.deliver_it.models.City;

import java.util.List;

public interface CityService {

    List<City> getAll();

    City getById(int id);
}
