package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Address;
import com.team007.deliver_it.models.User;
import com.team007.deliver_it.models.Warehouse;
import com.team007.deliver_it.repositories.WarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import static com.team007.deliver_it.models.User.*;

@Service
public class WarehouseServiceImpl implements WarehouseService {

    private final WarehouseRepository repository;
    private final AddressService addressService;

    @Autowired
    public WarehouseServiceImpl(WarehouseRepository repository, AddressService addressService) {
        this.repository = repository;
        this.addressService = addressService;
    }

    @Override
    public List<Warehouse> getAll() {
        return repository.getAll();
    }

    @Override
    public Warehouse getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void create(Warehouse warehouse, Address address) {
        addressService.create(address);
        repository.create(warehouse);
    }

    @Override
    public void update(Warehouse warehouse, Address address) {
        addressService.update(address);
        repository.update(warehouse);
    }

    @Override
    public void delete(int id) {
        Address warehouseAddress = getById(id).getAddress();
        repository.delete(id);
        addressService.delete(warehouseAddress);
    }
}
