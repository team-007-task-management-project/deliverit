package com.team007.deliver_it.services;

import com.team007.deliver_it.models.UserAddress;

public interface UserAddressService {
    void create(UserAddress userAddress);

    UserAddress getByUserId(int userId);

    void delete(UserAddress userAddress);
}
