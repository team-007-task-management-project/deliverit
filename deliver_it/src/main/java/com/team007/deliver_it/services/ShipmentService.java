package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Shipment;
import com.team007.deliver_it.models.User;

import java.util.List;
import java.util.Optional;

public interface ShipmentService {

    List<Shipment> getAll();

    Shipment getById(int id);

    List<Shipment> getByStatus(int statusId);

    Shipment getNextShipmentByDestinationWarehouseId(int destinationWarehouseId);

    List<Shipment> filter(Optional<Integer> statusId, Optional<Integer> warehouseId, Optional<Integer> userId);

    List<Shipment> filter(Optional<Integer> statusId, Optional<Integer> warehouseId);

    void create(Shipment shipment);

    void update(Shipment shipment);

    void delete(int id);
}
