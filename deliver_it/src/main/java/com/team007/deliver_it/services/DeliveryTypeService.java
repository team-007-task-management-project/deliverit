package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Country;
import com.team007.deliver_it.models.DeliveryType;
import java.util.List;

public interface DeliveryTypeService {

    List<DeliveryType> getAll();

    DeliveryType getById(int id);
}
