package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Role;

public interface RoleService {

    Role getById(int id);
}
