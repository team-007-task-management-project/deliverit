package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Address;
import com.team007.deliver_it.models.User;
import com.team007.deliver_it.models.Warehouse;

import java.util.List;

public interface WarehouseService {

    List<Warehouse> getAll();

    Warehouse getById(int id);

    void create(Warehouse warehouse, Address address);

    void update(Warehouse warehouse, Address address);

    void delete(int id);
}
