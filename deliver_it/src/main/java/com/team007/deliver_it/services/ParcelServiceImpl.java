package com.team007.deliver_it.services;

import com.team007.deliver_it.exceptions.UnauthorizedOperationException;
import com.team007.deliver_it.models.Parcel;
import com.team007.deliver_it.models.Status;
import com.team007.deliver_it.models.User;
import com.team007.deliver_it.repositories.ParcelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ParcelServiceImpl implements ParcelService {
    private static final String DEFAULT_DELIVERY_TYPE_ERROR_MESSAGE = "The default delivery type should always be 'Pick up from warehouse'.";
    private static final String PARCEL_USER_TYPE_ERROR_MESSAGE = "Only customers can be assigned to parcels.";
    private static final String DELIVERY_TYPE_CHANGE_ERROR_MESSAGE = "You cannot change the delivery types once the shipment status is 'Completed'.";
    private static final String DELIVERY_TYPE_UPDATE_USER_ERROR_MESSAGE = "You can only update your own parcel's delivery type.";
    private static final String PARCEL_UPDATE_ERROR_MESSAGE = "You cannot update a parcel which is already 'Completed'";
    private static final String PARCEL_GET_STATUS_ERROR_MESSAGE = "You can only view the status of your own parcels.";
    private static final int STATUS_COMPLETED = 3;
    private static final int DELIVERY_TYPE_PICK_UP_FROM_WAREHOUSE = 1;

    private final ParcelRepository repository;
    private final StatusService statusService;

    @Autowired
    public ParcelServiceImpl(ParcelRepository repository, StatusService statusService) {
        this.repository = repository;
        this.statusService = statusService;
    }

    @Override
    public List<Parcel> getAll() {
        return repository.getAll();
    }

    @Override
    public List<Parcel> getAllForUser(User user) {
        return repository.getAllForUser(user);
    }

    @Override
    public Parcel getById(int id) {
        return repository.getById(id);
    }

    @Override
    public List<Parcel> getByStatus(int statusId, int userId) {
        statusService.getById(statusId);
        return repository.getByStatus(statusId, userId);
    }

    @Override
    public Status getStatus(int parcelId, User authenticatedUser) {
        Parcel existingParcel = getById(parcelId);
        checkIfUserHasPermissionToUpdate(authenticatedUser, existingParcel);
        return repository.getStatus(parcelId);
    }

    @Override
    public List<Parcel> filter(Optional<Integer> userId, Optional<Integer> warehouseId, Optional<Integer> categoryId, Optional<Double> minWeight, Optional<Double> maxWeight, Optional<Integer> statusId) {
        return repository.filter(userId, warehouseId, categoryId, minWeight, maxWeight, statusId);
    }

    @Override
    public List<Parcel> sort(Optional<String> weight, Optional<String> arrivalDate) {
        return repository.sort(weight, arrivalDate);
    }

    @Override
    public void create(Parcel parcel) {
        checkInitialDeliveryTypeValid(parcel);
        checkParcelOwnerRole(parcel);
        repository.create(parcel);
    }

    @Override
    public void customerUpdate(User authenticatedUser, Parcel parcel) {
        checkPermissionToUpdateDeliveryType(authenticatedUser, parcel);
        checkShipmentStatusBeforeUpdating(parcel, DELIVERY_TYPE_CHANGE_ERROR_MESSAGE);
        repository.update(parcel);
    }


    @Override
    public void employeeUpdate(Parcel parcel) {
        Parcel existingParcel = getById(parcel.getId());
        checkShipmentStatusBeforeUpdating(existingParcel, PARCEL_UPDATE_ERROR_MESSAGE);

        checkParcelOwnerRole(parcel);
        repository.update(parcel);
    }

    @Override
    public void delete(int id) {
        repository.delete(id);
    }

    private void checkIfUserHasPermissionToUpdate(User authenticatedUser, Parcel existingParcel) {
        if (existingParcel.getUser().getId() != authenticatedUser.getId() && !authenticatedUser.getRole().getName().equals("Employee")) {
            throw new UnauthorizedOperationException(PARCEL_GET_STATUS_ERROR_MESSAGE);
        }
    }

    private void checkInitialDeliveryTypeValid(Parcel parcel) {
        if (parcel.getDeliveryType().getId() != DELIVERY_TYPE_PICK_UP_FROM_WAREHOUSE) {
            throw new IllegalArgumentException(DEFAULT_DELIVERY_TYPE_ERROR_MESSAGE);
        }
    }

    private void checkPermissionToUpdateDeliveryType(User authenticatedUser, Parcel parcel) {
        if (!parcel.getUser().getEmail().equals(authenticatedUser.getEmail())) {
            throw new UnauthorizedOperationException(DELIVERY_TYPE_UPDATE_USER_ERROR_MESSAGE);
        }
    }

    private void checkShipmentStatusBeforeUpdating(Parcel parcel, String errorMessage) {
        if (parcel.getShipment().getStatus().getId() == STATUS_COMPLETED) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    private void checkParcelOwnerRole(Parcel parcel) {
        if (!parcel.getUser().getRole().getName().equals("Customer")) {
            throw new IllegalArgumentException(PARCEL_USER_TYPE_ERROR_MESSAGE);
        }
    }
}
