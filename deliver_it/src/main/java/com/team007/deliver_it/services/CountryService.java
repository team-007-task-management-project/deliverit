package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Country;

import java.util.List;

public interface CountryService {

    List<Country> getAll();

    Country getById(int id);
}
