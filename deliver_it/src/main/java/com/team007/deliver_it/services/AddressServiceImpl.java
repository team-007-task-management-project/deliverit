package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Address;
import com.team007.deliver_it.repositories.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements AddressService {

    private static final String STREET_NAME_INVALID = "Street name cannot be null and must be between 1 and 50 symbols.";

    private final AddressRepository repository;

    @Autowired
    public AddressServiceImpl(AddressRepository repository) {
        this.repository = repository;
    }

    @Override
    public void create(Address address) {
        String streetName = address.getStreetName();
        createAddress(address, streetName);
    }

    @Override
    public Address getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void update(Address address) {
        String streetName = address.getStreetName();
        updateAddress(address, streetName);
    }

    @Override
    public void delete(Address address) {
        repository.delete(address);
    }

    private boolean isValid(String streetName) {
        return streetName != null && streetName.length() >= 1 && streetName.length() <= 50;
    }

    private void createAddress(Address address, String streetName) {
        if (isValid(streetName)) {
            repository.create(address);
        } else {
            throw new IllegalArgumentException(STREET_NAME_INVALID);
        }
    }

    private void updateAddress(Address address, String streetName) {
        if (isValid(streetName)) {
            repository.update(address);
        } else {
            throw new IllegalArgumentException(STREET_NAME_INVALID);
        }
    }
}
