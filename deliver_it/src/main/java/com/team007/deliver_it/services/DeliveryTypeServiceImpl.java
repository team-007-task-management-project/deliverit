package com.team007.deliver_it.services;

import com.team007.deliver_it.models.DeliveryType;
import com.team007.deliver_it.repositories.DeliveryTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeliveryTypeServiceImpl implements DeliveryTypeService {
    private final DeliveryTypeRepository repository;

    @Autowired
    public DeliveryTypeServiceImpl(DeliveryTypeRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<DeliveryType> getAll() {
        return repository.getAll();
    }

    @Override
    public DeliveryType getById(int id) {
        return repository.getById(id);
    }
}
