package com.team007.deliver_it.services;

import com.team007.deliver_it.models.UserAddress;
import com.team007.deliver_it.repositories.UserAddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserAddressServiceImpl implements UserAddressService {

    private final UserAddressRepository repository;

    @Autowired
    public UserAddressServiceImpl(UserAddressRepository repository) {
        this.repository = repository;
    }

    @Override
    public void create(UserAddress userAddress) {
        repository.create(userAddress);
    }

    @Override
    public UserAddress getByUserId(int userId) {
        return repository.getByUserID(userId);
    }

    @Override
    public void delete(UserAddress userAddress) {
        repository.delete(userAddress);
    }
}
