package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Role;
import com.team007.deliver_it.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository repository;

    @Autowired
    public RoleServiceImpl(RoleRepository repository) {
        this.repository = repository;
    }

    @Override
    public Role getById(int id) {
        return repository.getById(id);
    }
}
