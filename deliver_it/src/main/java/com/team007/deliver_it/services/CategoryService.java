package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAll();

    Category getById(int id);
}
