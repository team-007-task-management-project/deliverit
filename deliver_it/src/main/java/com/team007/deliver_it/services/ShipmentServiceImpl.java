package com.team007.deliver_it.services;

import com.team007.deliver_it.models.Shipment;
import com.team007.deliver_it.models.Warehouse;
import com.team007.deliver_it.repositories.ShipmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class ShipmentServiceImpl implements ShipmentService {

    private static final String SHIPMENT_STATUS_INITIAL_ERROR_MESSAGE = "Initial shipment status should always be 'Preparing'";
    private static final String SHIPMENT_EMPTY_ERROR_MESSAGE = "Shipment has no parcels, therefore cannot depart.";
    private static final String SHIPMENT_STATUS_UPDATE_ERROR_MESSAGE = "Status can move only forward.";
    private static final String SHIPMENT_DEPARTURE_DATE_ERROR_MESSAGE = "Departure date can only be in the present or future";
    private static final String SHIPMENT_ARRIVAL_DATE_ERROR_MESSAGE = "Arrival date should be in the future";
    private static final String SHIPMENT_WAREHOUSE_ERROR_MESSAGE = "Destination warehouse should not be the same as origin warehouse";
    private static final int STATUS_ON_THE_WAY = 2;
    private static final int STATUS_PREPARING = 1;
    private final ShipmentRepository repository;
    private final StatusService statusService;
    private final WarehouseService warehouseService;

    @Autowired
    public ShipmentServiceImpl(ShipmentRepository repository, StatusService statusService, WarehouseService warehouseService) {
        this.repository = repository;
        this.statusService = statusService;
        this.warehouseService = warehouseService;
    }

    @Override
    public List<Shipment> getAll() {
        return repository.getAll();
    }

    @Override
    public Shipment getById(int id) {
        return repository.getById(id);
    }

    @Override
    public List<Shipment> getByStatus(int statusId) {
        statusService.getById(statusId);
        return repository.getByStatus(statusId);
    }

    @Override
    public Shipment getNextShipmentByDestinationWarehouseId(int destinationWarehouseId) {
        Warehouse destinationWarehouse = warehouseService.getById(destinationWarehouseId);
        return repository.getNextShipmentByDestinationWarehouseId(destinationWarehouseId, destinationWarehouse);
    }

    @Override
    public List<Shipment> filter(Optional<Integer> statusId, Optional<Integer> warehouseId, Optional<Integer> userId) {
        return repository.filter(statusId, warehouseId, userId);
    }

    @Override
    public List<Shipment> filter(Optional<Integer> statusId, Optional<Integer> warehouseId) {
        return repository.filter(statusId, warehouseId);
    }

    @Override
    public void create(Shipment shipment) {
        checkInitialShipmentStatus(shipment);
        validateShipment(shipment);
        repository.create(shipment);
    }

    @Override
    public void update(Shipment shipment) {
        Shipment existingShipment = getById(shipment.getId());
        int shipmentStatus = existingShipment.getStatus().getId();
        checkIfShipmentHasParcelsBeforeChangingStatus(shipment);
        checkIfShipmentStatusIsReceding(shipment, shipmentStatus);
        validateShipment(shipment);
        repository.update(shipment);
    }


    @Override
    public void delete(int id) {
        repository.delete(id);
    }

    private void validateShipment(Shipment shipment) {
        if (shipment.getDepartureDate().isBefore(LocalDate.now())) {
            throw new IllegalArgumentException(SHIPMENT_DEPARTURE_DATE_ERROR_MESSAGE);
        }
        if (shipment.getArrivalDate().isBefore(shipment.getDepartureDate())) {
            throw new IllegalArgumentException(SHIPMENT_ARRIVAL_DATE_ERROR_MESSAGE);
        }
        if (shipment.getOriginWarehouse().getId() == shipment.getDestinationWarehouse().getId()) {
            throw new IllegalArgumentException(SHIPMENT_WAREHOUSE_ERROR_MESSAGE);
        }
    }

    private void checkInitialShipmentStatus(Shipment shipment) {
        if (shipment.getStatus().getId() != STATUS_PREPARING) {
            throw new IllegalArgumentException(SHIPMENT_STATUS_INITIAL_ERROR_MESSAGE);
        }
    }

    private void checkIfShipmentHasParcelsBeforeChangingStatus(Shipment shipment) {
        if (shipment.getStatus().getId() >= STATUS_ON_THE_WAY && !repository.hasParcels(shipment)) {
            throw new IllegalArgumentException(SHIPMENT_EMPTY_ERROR_MESSAGE);
        }
    }

    private void checkIfShipmentStatusIsReceding(Shipment shipment, int shipmentStatus) {
        if (shipment.getStatus().getId() < shipmentStatus) {
            throw new IllegalArgumentException(SHIPMENT_STATUS_UPDATE_ERROR_MESSAGE);
        }
    }
}
