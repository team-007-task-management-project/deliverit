package com.team007.deliver_it.services;

import com.team007.deliver_it.exceptions.*;
import com.team007.deliver_it.mappers.AddressMapper;
import com.team007.deliver_it.models.*;
import com.team007.deliver_it.models.dtos.UserDto;
import com.team007.deliver_it.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private static final String DELETE_USERS_ERROR_MESSAGE = "Users can only be deleted by the users themselves or by an employee.";
    private static final String UPDATE_USER_ERROR_MESSAGE = "Personal information can only be updated by the users themselves or by an employee.";
    private static final String PARCELS_INCOMING_GET_ERROR_MESSAGE = "Incoming parcels can only be viewed by the parcel owners themselves or employees.";

    private final UserRepository repository;
    private final UserAddressService userAddressService;
    private final AddressService addressService;
    private final AddressMapper addressMapper;

    @Autowired
    public UserServiceImpl(UserRepository repository, UserAddressService userAddressService, AddressService addressService, AddressMapper addressMapper) {
        this.repository = repository;
        this.userAddressService = userAddressService;
        this.addressService = addressService;
        this.addressMapper = addressMapper;
    }

    @Override
    public List<User> getAll() {
        return repository.getAll();
    }

    @Override
    public List<User> getAllCustomers() {
        return repository.getAllCustomers();
    }

    @Override
    public List<User> getAllEmployees() {
        return repository.getAllEmployees();
    }

    @Override
    public User getById(int id) {
        return repository.getById(id);
    }

    @Override
    public User getByEmail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public List<Parcel> getCustomerIncomingParcels(User authenticatedUser, int customerId) {
        User existingUser = getById(customerId);
        checkUserPermission(existingUser, authenticatedUser, PARCELS_INCOMING_GET_ERROR_MESSAGE);
        return repository.getCustomerIncomingParcels(customerId);
    }

    @Override
    public List<User> filter(Optional<String> firstName, Optional<String> lastName, Optional<String> email) {
        return repository.filter(firstName, lastName, email);
    }

    @Override
    public List<User> search(Optional<String> keyWord) {
        return repository.search(keyWord);
    }

    @Override
    public void createCustomer(User user, UserDto userDto) {
        checkDuplicateEmail(user);
        Address address = createAddress(userDto);
        repository.createCustomer(user);
        createUserAddress(user, address);
    }

    @Override
    public void createEmployee(User user, UserDto userDto) {
        checkDuplicateEmail(user);
        repository.createEmployee(user);
        createAddress(user, userDto);
    }


    @Override
    public void update(User user, User authenticatedUser, UserDto userDto) {
        checkDuplicateEmail(user);
        checkUserPermission(user, authenticatedUser, UPDATE_USER_ERROR_MESSAGE);
        repository.update(user);
        if (hasAddress(userDto)) {
            UserAddress userAddress = userAddressService.getByUserId(user.getId());
            if (userAddress == null) {
                Address address = createAddress(userDto);
                createUserAddress(user, address);
            } else {
                Address address = addressMapper.updateAddress(userAddress, userDto);
                addressService.update(address);
            }
        }
    }

    @Override
    public void delete(int id, User authenticatedUser) {
        User existingUser = getById(id);
        UserAddress userAddress = userAddressService.getByUserId(id);
        Address address = userAddress.getAddress();
        checkUserPermission(existingUser, authenticatedUser, DELETE_USERS_ERROR_MESSAGE);
        userAddressService.delete(userAddress);
        addressService.delete(address);

        repository.delete(id);
    }

    private Address createAddress(UserDto userDto) {
        Address address = addressMapper.fromUserDtoToAddressObject(userDto);
        addressService.create(address);
        return address;
    }

    private void createUserAddress(User user, Address address) {
        UserAddress userAddress = new UserAddress();
        userAddress.setUser(user);
        userAddress.setAddress(address);
        userAddressService.create(userAddress);
    }

    private void checkUserPermission(User user, User authenticatedUser, String updateUserErrorMessage) {
        if (user.getId() != (authenticatedUser.getId()) && !authenticatedUser.getRole().getName().equals("Employee")) {
            throw new UnauthorizedOperationException(updateUserErrorMessage);
        }
    }

    private void checkDuplicateEmail(User user) {
        boolean duplicateExists = true;
        try {
            User existingUser = repository.getByEmail(user.getEmail());
            if (existingUser.getId() == user.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }
    }

    private void createAddress(User user, UserDto userDto) {
        if (hasAddress(userDto)) {
            Address address = createAddress(userDto);
            createUserAddress(user, address);
        }
    }

    private boolean hasAddress(UserDto userDto) {
        return userDto.getStreetName() != null && userDto.getCityId() != 0;
    }
}
