# DeliverIT - We bring the store to your door!

## We are a delivery company that will help you get your favourite and most wanted item delivered in no time :)

### Swagger: http://localhost:8080/swagger-ui/index.html#/

### How to fill your database: You must first run the create.sql file in order to create the schema, tables and relations and then run the insert-data.sql file in order to fill the database with data.

### Database Relations Image: ![](deliver_it/db/DeliverIT_DB_Relations.png)
